package model.ball;

import element.Point2D;

/**
 * Interface for the builder of the ball.
 *
 */
public interface BallBuilder {

    /**
     * Interface for the movement of the ball.
     *
     */
    interface Movement {
        /**
         *
         * @param ball the ball moved
         */
        void ballMovement(Ball ball);
    }

    /**
     * Interface for the stop of the ball.
     *
     */
    interface Stop {
        /**
         *
         * @param ball the ball stopped
         */
        void ballStop(Ball ball);
    }


    /**
     * @return the ball build with the params add
     */

    Ball build();

    /**
     * @param radius the radius of the ball
     * @return a BallBuilder
     */

    BallBuilder addRadius(double radius);

    /**
     * @param damage the damage of the ball
     * @return a BallBuilder
     */

    BallBuilder addDamage(int damage);

    /**
     * @param startSpeed the start speed of the ball
     * @return a BallBuilder
     */

    BallBuilder addStartSpeed(double startSpeed);

    /**
     * @param incrementSpeedPercent the percent of the initial speed to increment
     * @return a BallBuilder
     */
    BallBuilder addIncrementSpeed(double incrementSpeedPercent);

    /**
     * @param incrementSpeedStep the number of the step after that the speed is increment
     * @return a BallBuilder
     */
    BallBuilder addIncrementSpeedStep(int incrementSpeedStep);

    /**
     * @param speedLimit the limit of the ball speed
     * @return a BallBuilder
     */
    BallBuilder addSpeedLimit(double speedLimit);

    /**
     * @param startPosition the start position of the ball
     * @return a BallBuilder
     */

    BallBuilder addStartPosition(Point2D startPosition);

    /**
     * @param movement the method for movement
     * @return a BallBuilder
     */

    BallBuilder addMovement(Movement movement);

    /**
     * @param stop the method for stop
     * @return a BallBuilder
     */

    BallBuilder addStop(Stop stop);

    /**
     * the ball can collide with other Object.
     *
     * @return a BallBuilder
     */
    BallBuilder addBallCanCollide();

    /**
     * the ball can't collide with other Object.
     *
     * @return a BallBuilder
     */
    BallBuilder addBallCanNotCollide();
}
