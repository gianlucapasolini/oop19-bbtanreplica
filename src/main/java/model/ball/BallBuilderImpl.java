package model.ball;

import element.Elements;
import element.Point2D;
import element.Vector2D;
import utility.Counter;
import utility.DoubleCounterFactory;
import utility.IntegerCounterFactory;
import utility.LimitCounterFactory;
import utility.LimitCounterFactoryImpl;

import java.util.Objects;
import java.util.Optional;

/**
 * Builder for ball.
 *
 */
public class BallBuilderImpl implements BallBuilder {

    private static final double STANDARD_SPEED_INCREMENT = 5;
    private static final double STANDARD_SPEED_LIMIT = 200;
    private static final int STANDARD_INCREMENT_SPEED_STEP = 20;

    private double radius = 1;
    private int damage = 1;
    private double startSpeed = 1;
    private double incrementSpeed = STANDARD_SPEED_INCREMENT;
    private double speedLimit = STANDARD_SPEED_LIMIT;
    private int incrementSpeedStep = STANDARD_INCREMENT_SPEED_STEP;
    private Movement movement;
    private Stop stop;
    private Point2D startPosition;
    private boolean ballAbleToCollide = true;

    /**
     * @return a ball with the selected parameters
     */
    @Override
    public Ball build() {
        if (Objects.isNull(this.startPosition) || speedLimit < this.startSpeed) {
            throw new IllegalStateException();
        }
        final LimitCounterFactory limit = new LimitCounterFactoryImpl();
        final double speedIncrement = startSpeed * incrementSpeed / 100;
        if (Objects.isNull(this.stop)) {
            this.stop = ball -> {
            };
        }
        if (Objects.isNull(this.movement)) {
            this.movement = ball -> {
            };
        }
        return new Ball() {

            private final Counter<Double> speed = limit.limitCounter(new DoubleCounterFactory().counterFromValueWithStep(startSpeed, speedIncrement), speedLimit);
            private final Counter<Integer> step = limit.limitCounterNotifyAndReset(new IntegerCounterFactory().standardCounter(), incrementSpeedStep, (source, argument) -> this.incrementSpeed());
            private Point2D position = startPosition;
            private Vector2D direction = Elements.VECTOR_NULL;
            private Optional<Point2D> destination = Optional.empty();
            private boolean isAbleToCollide = ballAbleToCollide;
            private boolean insideAnotherObject;

            @Override
            public double getRadius() {
                return radius;
            }

            @Override
            public synchronized double getSpeed() {
                return this.direction.isNullVector() ? 0 : this.speed.getValue();
            }

            @Override
            public synchronized void incrementSpeed() {
                this.speed.increment();
            }

            @Override
            public synchronized void resetSpeed() {
                this.speed.reset();
            }

            @Override
            public int getDamage() {
                return damage;
            }

            @Override
            public synchronized Point2D getPosition() {
                return this.position;
            }

            @Override
            public synchronized Vector2D getDirection() {
                return this.direction;
            }

            @Override
            public synchronized void setDirection(final Vector2D newDirection) {
                if (Objects.isNull(newDirection)) {
                    throw new IllegalArgumentException();
                }
                if (!this.direction.equals(newDirection)) {
                    this.direction = newDirection;
                    if (this.direction.isNullVector()) {
                        this.resetSpeed();
                        this.step.reset();
                        stop.ballStop(this);
                    } else {
                        this.direction = this.direction.getNormalizedVector();
                    }
                }
            }

            @Override
            public synchronized boolean isStationary() {
                return this.direction.isNullVector();
            }

            @Override
            public synchronized void moveByTime(final double timeInterval) {
                if (!this.isStationary() && timeInterval != 0) {
                    this.moveByDistance(timeInterval * this.getSpeed());
                    this.step.increment();
                }
            }

            @Override
            public synchronized void moveByDistance(final double distance) {
                if (this.destination.isPresent()) {
                    this.reachDestination(distance);
                }
                this.move(this.direction.scalarMultiplication(distance));
            }

            private void reachDestination(final double distance) {
                if (this.destination.isPresent()) {
                    final Vector2D difference = this.destination.get().subtraction(this.position);
                    if (this.isDestinationReachable(difference, distance)) {
                        this.move(difference);
                        this.setDirection(Elements.VECTOR_NULL);
                        this.destination = Optional.empty();
                    }
                }
            }

            private boolean isDestinationReachable(final Vector2D difference, final double distance) {
                return difference.hasSameNormalizedVector(this.direction) && difference.getModulus() <= distance;
            }

            private void move(final Vector2D distance) {
                if (!distance.isNullVector()) {
                    this.position = this.position.sum(distance);
                    movement.ballMovement(this);
                }
            }

            @Override
            public synchronized void collision(final Point2D centerPoint, final Vector2D newDirection) {
                if (Objects.isNull(centerPoint) || Objects.isNull(newDirection)) {
                    throw new IllegalArgumentException();
                }
                if (this.isAbleToCollide && !centerPoint.equals(this.position)) {
                    final Vector2D distance = this.position.subtraction(centerPoint);
                    if (this.isSameNormalizedVectorNotNull(distance)) {
                        throw new IllegalArgumentException();
                    }
                    this.position = centerPoint;
                    this.setDirection(newDirection);
                    this.moveByDistance(distance.getModulus());
                }
            }

            private boolean isSameNormalizedVectorNotNull(final Vector2D distance) {
                return distance.isNullVector() || this.direction.isNullVector()
                        || !distance.hasSameNormalizedVector(this.direction);
            }

            @Override
            public synchronized void setDestination(final Point2D destination) {
                if (this.position.equals(destination)) {
                    this.setDirection(Elements.VECTOR_NULL);
                } else {
                    this.destination = Optional.of(destination);
                }
            }

            @Override
            public boolean isAbleToCollide() {
                return this.isAbleToCollide;
            }

            @Override
            public void notAbleToCollide() {
                this.isAbleToCollide = false;
            }

            @Override
            public void ableToCollide() {
                this.isAbleToCollide = true;
            }

            @Override
            public boolean isInsideAnotherObject() {
                return insideAnotherObject;
            }

            @Override
            public void insideAnotherObject() {
                insideAnotherObject = true;
            }

            @Override
            public void notInsideAnotherObject() {
                insideAnotherObject = false;
            }
        };
    }

    /**
     * @param radius the radius of the ball
     * @return the BallBuilder
     */
    @Override
    public BallBuilder addRadius(final double radius) {
        if (radius <= 0) {
            throw new IllegalArgumentException();
        }
        this.radius = radius;
        return this;
    }

    /**
     * @param damage the damage of the ball
     * @return the BallBuilder
     */
    @Override
    public BallBuilder addDamage(final int damage) {
        if (damage <= 0) {
            throw new IllegalArgumentException();
        }
        this.damage = damage;
        return this;
    }

    /**
     * @param startSpeed the start speed of the ball
     * @return the BallBuilder
     */
    @Override
    public BallBuilder addStartSpeed(final double startSpeed) {
        if (startSpeed <= 0) {
            throw new IllegalArgumentException();
        }
        this.startSpeed = startSpeed;
        return this;
    }

    /**
     * @param incrementSpeedPercent the percent of the initial speed to increment
     * @return the BallBuilder
     */
    @Override
    public BallBuilder addIncrementSpeed(final double incrementSpeedPercent) {
        if (startSpeed <= 0 && incrementSpeedPercent < 0) {
            throw new IllegalArgumentException();
        }
        this.incrementSpeed = incrementSpeedPercent;
        return this;
    }

    /**
     * @param incrementSpeedStep the number of the step after that the speed is increment
     * @return the BallBuilder
     */
    @Override
    public BallBuilder addIncrementSpeedStep(final int incrementSpeedStep) {
        if (incrementSpeedStep <= 10) {
            throw new IllegalArgumentException();
        }
        this.incrementSpeedStep = incrementSpeedStep;
        return this;
    }

    /**
     * @param speedLimit the limit of the ball speed
     * @return the BallBuilder
     */
    @Override
    public BallBuilder addSpeedLimit(final double speedLimit) {
        if (speedLimit < this.startSpeed) {
            throw new IllegalArgumentException();
        }
        this.speedLimit = speedLimit;
        return this;
    }

    /**
     * @param startPosition the start position of the ball
     * @return the BallBuilder
     */
    @Override
    public BallBuilder addStartPosition(final Point2D startPosition) {
        if (Objects.isNull(startPosition)) {
            throw new IllegalArgumentException();
        }
        this.startPosition = startPosition;
        return this;
    }

    /**
     * @param movement the method for movement
     * @return the BallBuilder
     */
    @Override
    public BallBuilder addMovement(final Movement movement) {
        if (Objects.isNull(movement)) {
            throw new IllegalArgumentException();
        }
        this.movement = movement;
        return this;
    }

    /**
     * @param stop the method for stop
     * @return the BallBuilder
     */
    @Override
    public BallBuilder addStop(final Stop stop) {
        if (Objects.isNull(stop)) {
            throw new IllegalArgumentException();
        }
        this.stop = stop;
        return this;
    }

    /**
     * the ball can collide.
     *
     * @return the BallBuilder
     */
    @Override
    public BallBuilder addBallCanCollide() {
        this.ballAbleToCollide = true;
        return this;
    }

    /**
     * the ball can't collide.
     *
     * @return the BallBuilder
     */
    @Override
    public BallBuilder addBallCanNotCollide() {
        this.ballAbleToCollide = false;
        return this;
    }
}
