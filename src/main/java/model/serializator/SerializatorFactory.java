package model.serializator;

import model.player.Player;
import model.player.PlayersCollection;

/**
 * This interface model a Factory for Serializator interface: this create different type of serializator pre-built
 * and ready to use.
 */
public interface SerializatorFactory {

    /**
     * This method creates a simple player serializator.
     *
     * @return Return the player serializator in order to serialize ONLY player object
     */
    Serializator<Player> playerSerializator();

    /**
     * This method creates a simple PlayersCollection serializator.
     *
     * @return Return the player serializator in order to serialize ONLY PlayersCollection object
     */
    Serializator<PlayersCollection> playersCollectionSerializator();

    /**
     * This method creates a secure player serializator/deserializator ad hoc, that control even players info are corrupted.
     *
     * @return The secure player serializator/deserializator using Decorator Pattern
     */
    Serializator<PlayersCollection> securePlayersCollectionSerializator();
}
