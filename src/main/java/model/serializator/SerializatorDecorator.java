package model.serializator;

import model.player.PlayersCollection;
import org.json.simple.JSONObject;

/**
 * This is an abstract class created for implementing Decorator Pattern: this class can be extended and
 * decorated with new functionality.
 */
public abstract class SerializatorDecorator implements Serializator<PlayersCollection> {

    /**
     * This field represent the object wrapped into the class of the same basic type in order
     * to apply the Decorator pattern: the goal is to add additional responsibility.
     */
    private final Serializator<PlayersCollection> serializator;

    protected SerializatorDecorator(final Serializator<PlayersCollection> serializator) {
        this.serializator = serializator;
    }

    /**
     * This method is a simple getter used to give access to the hereditary classes.
     * @return Return the serializator object wrapped into this abstract class.
     */
    protected Serializator<PlayersCollection> getSerializator() {
        return this.serializator;
    }

    /**
     * This method implicitly use the serializator's object and its methods to perform those features that must be the same
     * in according to Decorator pattern, and extending this class can be added other features.
     * @param object The object of generic type X to be converted into json object.
     * @return Return the json object that corresponds to the object converted using serializator methods.
     */
    @Override
    public JSONObject convertIntoJSONObject(final PlayersCollection object) {
        return this.serializator.convertIntoJSONObject(object);
    }

    /**
     *
     * @param object The json object that you want to deserialize, converting it into its generic type X.
     * @return Return the PlayersCollection that corresponds to the JSONObject using deserialization methods.
     */
    @Override
    public PlayersCollection deserializeJSONObject(final JSONObject object) {
        return serializator.deserializeJSONObject(object);
    }

}
