package model.serializator;

import utility.jsonbuilder.JSONObjectBuilder;
import utility.jsonbuilder.JSONObjectBuilderImpl;
import model.player.Player;
import model.player.PlayersCollection;
import model.player.PlayersCollectionImpl;
import model.player.PlayerBuilderImpl;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import utility.BallColor;

import java.util.Set;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class implements the Serializator interface.
 */
public final class SerializatorFactoryImpl implements SerializatorFactory {

    @Override
    public Serializator<Player> playerSerializator() {
        return new Serializator<>() {

            @SuppressWarnings("unchecked")
            @Override
            public JSONObject convertIntoJSONObject(final Player player) {
                final JSONArray jsonUnlockedRadius = new JSONArray();
                final JSONArray jsonUnlockedColors = new JSONArray();
                jsonUnlockedRadius.addAll(player.getUnlockedRadius());
                player.getColors().forEach(i -> jsonUnlockedColors.add(i.getIndex()));
                final JSONObjectBuilder jsonBuilder = new JSONObjectBuilderImpl();
                jsonBuilder.put("Name", player.getName())
                        .put("BestScore", player.getBestScore())
                        .put("LastScore", player.getLastScore())
                        .put("Money", player.getCurrentMoney())
                        .put("CurrentBallRadius", player.getCurrentBallRadius())
                        .put("CurrentBallDamage", player.getCurrentBallDamage())
                        .put("CurrentColor", player.getCurrentColor().getIndex())
                        .put("UnlockedRadius", jsonUnlockedRadius)
                        .put("UnlockedColors", jsonUnlockedColors);
                return jsonBuilder.build();
            }

            @Override
            public Player deserializeJSONObject(final JSONObject jsonObject) {
                final String name = (String) jsonObject.get("Name");
                final int score = ((Long) jsonObject.get("BestScore")).intValue();
                final int lastScore = ((Long) jsonObject.get("LastScore")).intValue();
                final int money = ((Long) jsonObject.get("Money")).intValue();
                final int currentBallRadius = ((Long) jsonObject.get("CurrentBallRadius")).intValue();
                final int currentBallDamage = ((Long) jsonObject.get("CurrentBallDamage")).intValue();
                final BallColor currentColor = BallColor.getColorByIndex(((Long) jsonObject.get("CurrentColor")).intValue());

                Set<Integer> radiusSet;
                Set<BallColor> colorsSet;

                radiusSet = SerializationMethods.deserializeSimpleJsonArray(jsonObject, "UnlockedRadius");
                colorsSet = SerializationMethods.deserializeSimpleJsonArray(jsonObject, "UnlockedColors")
                        .stream().map(BallColor::getColorByIndex).collect(Collectors.toSet());

                return new PlayerBuilderImpl().addNamePlayer(name)
                        .addBestScore(score)
                        .addInitialMoney(money)
                        .addLastScore(lastScore)
                        .addUnlockedColors(currentColor, colorsSet)
                        .addUnlockedRadius(currentBallRadius, radiusSet)
                        .addDamage(currentBallDamage)
                        .build();
            }
        };
    }

    @SuppressWarnings("unchecked")
    @Override
    public Serializator<PlayersCollection> playersCollectionSerializator() {
        return new Serializator<>() {
            @Override
            public JSONObject convertIntoJSONObject(final PlayersCollection object) {
                final JSONObjectBuilder jsonObjectBuilder = new JSONObjectBuilderImpl();
                jsonObjectBuilder.put("CurrentPlayer", object.getCurrentPlayerName());
                final JSONArray jsonPlayers = new JSONArray();
                if (object.getPlayers().isEmpty()) {
                    throw new IllegalStateException();
                }
                for (final var p : object.getPlayers().get()) {
                    jsonPlayers.add(playerSerializator().convertIntoJSONObject(p));
                }
                jsonObjectBuilder.put("Players", jsonPlayers);
                return jsonObjectBuilder.build();
            }

            @Override
            public PlayersCollection deserializeJSONObject(final JSONObject object) {
                final PlayersCollection pc = new PlayersCollectionImpl();
                final Serializator<Player> playerSerializator = new SerializatorFactoryImpl().playerSerializator();
                final String name = (String) object.get("CurrentPlayer");
                final List<Player> playersList = new ArrayList<>();

                final JSONArray playersListArray = (JSONArray) object.get("Players");
                for (final var p : playersListArray) {
                    playersList.add(playerSerializator.deserializeJSONObject((JSONObject) p));
                }
                pc.setCurrentPlayer(name);
                pc.setPlayers(playersList);
                return pc;
            }
        };
    }

    @Override
    public Serializator<PlayersCollection> securePlayersCollectionSerializator() {
        return new SecurePlayersSerializator(this.playersCollectionSerializator());
    }

}
