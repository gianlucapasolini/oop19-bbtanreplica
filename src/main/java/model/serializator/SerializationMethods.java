package model.serializator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * This is a simple class that collect static methods that perform some simple serialization/deserialization job.
 */
public final class SerializationMethods {

    private SerializationMethods() {
    }

    /**
     * This method serialize a simple generic array of object using reflections.
     *
     * @param object  Is the array you want to serialize into json.
     * @param path    Is the path you want to save the json file created.
     * @param getters The getters are the name of the getter methods of that values you want to convert into json.
     */
    @SuppressWarnings("unchecked")
    public static void serializeSimpleArray(final Object[] object, final String path, final String... getters) {
        if (object.getClass().isArray()) {
            try {
                final JSONObject obj = new JSONObject();
                for (int i = 0; i < Array.getLength(object); i++) {
                    Object[] attributes = new Object[2];
                    int cont = 0;
                    for (final String getter : getters) {
                        final Object o = Array.get(object, i);
                        final Method m = Array.get(object, i).getClass().getMethod("get" + getter);
                        attributes[cont] = m.invoke(o);
                        cont++;
                    }
                    obj.put(attributes[0].toString(), attributes[1]);
                }
                final FileWriter file = new FileWriter(path);
                file.write(obj.toJSONString());
                file.close();
            } catch (IOException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method deserialize a simple json array into a Set of Integer.
     *
     * @param jsonObj Is the json object to be deserialized into a Set of Integer.
     * @param name    Is the name of the json array saved into the file.
     * @return Return the set of Integer already created.
     */
    @SuppressWarnings("unchecked")
    public static Set<Integer> deserializeSimpleJsonArray(final JSONObject jsonObj, final String name) {
        final Set<Integer> integerSet = new HashSet<>();
        final JSONArray radiusSetArray = (JSONArray) jsonObj.get(name);
        final Iterator<Long> iterator = radiusSetArray.iterator();
        while (iterator.hasNext()) {
            integerSet.add(iterator.next().intValue());
        }
        return integerSet;
    }
}
