package model.serializator;

import javafx.util.Pair;
import model.player.Player;
import model.player.PlayersCollection;
import model.player.PlayersCollectionImpl;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.tinylog.Logger;

import java.io.IOException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;
import java.util.Optional;

/**
 * This class uses Decorator Pattern: it extends SerializatorDecorator class. In this way is created a decorator of PlayersCollection Serializator
 * that uses some of its functions and overrides two important functionality: the serialization and deserialization,
 * in order to implements a player-corrupted control, discarding those players which data are manually modified.
 */
public final class SecurePlayersSerializator extends SerializatorDecorator {

    public SecurePlayersSerializator(final Serializator<PlayersCollection> serializator) {
        super(serializator);
    }

    @Override
    public void serialize(final String path, final PlayersCollection object, final String fileName) throws IOException {
        SerializationMethods.serializeSimpleArray(object.getPlayers().get().stream().
                map(i -> new Pair<>(i.getName(), getSecureValue(i)))
                .toArray(), path + System.getProperty("file.separator") + "secureCodes.json", "Key", "Value");

        this.getSerializator().serialize(path, object, fileName);
    }

    @Override
    public Optional<PlayersCollection> deserialize(final String path, final String fileName) throws IOException, ParseException {
        final Optional<PlayersCollection> optOc = this.getSerializator().deserialize(path, fileName);
        if (optOc.isEmpty()) {
            return Optional.empty();
        }
        final PlayersCollection pc = optOc.get();
        final PlayersCollection secureCollection = new PlayersCollectionImpl();

        final JSONParser parser = new JSONParser();

        try (Reader reader = new FileReader(path + System.getProperty("file.separator") + "secureCodes.json")) {
            final JSONObject jsonObject = (JSONObject) parser.parse(reader);

            for (final Player player : pc.getPlayers().get()) {
                final int secureValue = ((Long) jsonObject.get(player.getName())).intValue();
                if (secureValue == getSecureValue(player)) {
                    secureCollection.addPlayer(player);
                } else {
                    Logger.warn("WARNING! Found corrupted player: " + player.getName());
                }
            }

            if (secureCollection.containsPlayer(pc.getCurrentPlayerName())) {
                secureCollection.setCurrentPlayer(pc.getCurrentPlayerName());
            }
            secureCollection.update();
            return Optional.of(secureCollection);
        } catch (IOException | ParseException e) {
            if (e instanceof FileNotFoundException) {
                return Optional.empty();
            }
            Logger.error("ERROR! Cannot open or parse secure codes file");
            throw e;
        }
    }

    /**
     * This method is a simple function that return a secure value created from a single player.
     * @param player Is the player who you want to calculate the secure value.
     * @return Return the secure value for player.
     */
    private int getSecureValue(final Player player) {
        return player.getBestScore() + player.getName().length() + player.getCurrentMoney();
    }

}
