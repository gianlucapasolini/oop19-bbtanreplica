package model.block;

/**
 * Interface used for logic Block
 */
public interface Block {

    /**
     * @return the value of BlockSize
     * getter of the BlockSize
     */
    double getBlockSize();

    /**
     * @return the value of ValueBlock
     * getter of the ValueBlock
     */
    int getValueBlock();

    /**
     * @param valueBlock is the 'life' of the block
     * setter of ValueBlock
     */
    void setValueBlock(int valueBlock);

}
