package model.block;

import controller.gamecontroller.GameControllerImpl;
import controller.viewcontroller.ViewGameControllerImpl;

/**
 * This class implements BlockView and used for logic block
 */
public class BlockImpl implements Block {

    private final double blockSize;

    private int valueBlock;

    public BlockImpl(final int valueBlock, final double strokeWidth) {
        this.valueBlock = valueBlock;
        this.blockSize = new ViewGameControllerImpl().getBlockPaneWidth() / new GameControllerImpl().getMaxBlockOnLine() - strokeWidth;
    }

    /**
     * @return the value of BlockSize
     * getter of the BlockSize
     */
    @Override
    public double getBlockSize() {
        return this.blockSize;
    }

    /**
     * @return the value of ValueBlock
     * getter of the ValueBlock
     */
    @Override
    public int getValueBlock() {
        return this.valueBlock;
    }

    /**
     * @param valueBlock is the 'life' of the block
     * setter of ValueBlock
     */
    @Override
    public void setValueBlock(final int valueBlock) {
        this.valueBlock = valueBlock;
    }

}
