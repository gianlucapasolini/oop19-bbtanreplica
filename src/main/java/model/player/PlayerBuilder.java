package model.player;

import utility.BallColor;

import java.util.Set;

/**
 * This interface model a builder for the player using the Builder Pattern.
 */
public interface PlayerBuilder {

    /**
     * @param name Name of the player you want to build
     * @return Return the PlayerBuilder with name set
     */
    PlayerBuilder addNamePlayer(String name);

    /**
     * @param bestScore The best score to be added for the new Player
     * @return Return the PlayerBuilder with best score set
     */
    PlayerBuilder addBestScore(int bestScore);

    /**
     * @param money Initial amount of money of the player
     * @return Return the PlayerBuilder with best score set
     */
    PlayerBuilder addInitialMoney(int money);

    /**
     * @param lastScore The last score done by the user
     * @return Return the PlayerBuilder with last score set
     */
    PlayerBuilder addLastScore(int lastScore);

    /**
     * @param color The color to be set as current color
     * @param unlockedColors The unlocked ball colors set winned by the user
     * @return Return the PlayerBuilder with unlocked colors set
     */
    PlayerBuilder addUnlockedColors(BallColor color, Set<BallColor> unlockedColors);

    /**
     * @param radius The radius to be set as current radius
     * @param unlockedRadius The unlocked ball radius set unlocked by the user
     * @return Return the PlayerBuilder with unlocked radius set
     */
    PlayerBuilder addUnlockedRadius(Integer radius, Set<Integer> unlockedRadius);

    /**
     * @param damage The damage value unlocked by the user
     * @return Return the PlayerBuilder with unlocked damage set
     */
    PlayerBuilder addDamage(int damage);

    /**
     * @return Return if all ok the Player built, making this object useless
     */
    Player build();
}
