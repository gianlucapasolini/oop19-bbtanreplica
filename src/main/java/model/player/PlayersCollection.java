package model.player;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * This class is a Players Collection that abstract the leaderboard game and manage the Players
 * registered, offering a simple way to insert and searching all the players.
 */
public interface PlayersCollection {

    /**
     * @return the player with the best score
     */
    Optional<Player> getBestPlayer();

    /**
     * @param name the name of the player to be returned
     * @return the player with that name
     */
    Optional<Player> getPlayer(String name);

    /**
     * @return return the list of the Player currently registered
     */
    Optional<List<Player>> getPlayers();

    /**
     * @param player the Player to be added in the PlayersCollection
     */
    void addPlayer(Player player);

    /**
     * @param name the name of the player to be added
     */
    void addPlayer(String name);

    /**
     * @return return the current Player. Optional.Empty is returned if there's no current player set
     */
    Optional<Player> getCurrentPlayer();

    /**
     * @param name the name of the Player to be set as current Player
     */
    void setCurrentPlayer(String name);

    /**
     * @param name the name of the Player we want to check that's contained
     * @return true if player with this name is present in list, false otherwise
     */
    boolean containsPlayer(String name);

    /**
     * Update method to save all modify performed.
     * @throws IOException Throw an IOException if any error occurred updating the collection.
     */
    void update() throws IOException;

    /**
     * @return Return the ordered by score list of Player currently saved in leaderboard-
     */
    Optional<List<Player>> getPlayersInOrder();

    /**
     * Logout method, set empty the current player.
     * @throws IOException Throw an IOException if any error occurred updating the collection.
     */
    void logout() throws IOException;

    /**
     * @return return the current Player's name
     */
    String getCurrentPlayerName();

    /**
     * Reset method that restore the leaderboard as empty collection.
     */
    void reset();

    /**
     * @param list List players to be set as current list of players registered
     */
    void setPlayers(List<Player> list);

}
