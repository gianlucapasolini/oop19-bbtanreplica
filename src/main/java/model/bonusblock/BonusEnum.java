package model.bonusblock;

/**
 * The interface representing the value inside type of the bonus.
 */
public interface BonusEnum {

    /**
     * @return the radius.
     */
    double getRadius();

    /**
     * @return the string rapresenting the path of the img.
     */
    String getImg();

    /**
     * @return return whether or not the bonus is destroyed after getting hit once.
     */
    boolean isInstatDelete();

}

