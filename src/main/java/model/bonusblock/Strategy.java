package model.bonusblock;

/**
 * Functional interface used for the strategy pattern.
 *
 * @param <B> the bonus that use the method.
 */
@FunctionalInterface
public interface Strategy<B extends BonusBlock> {

    /**
     * The action performed on hit.
     *
     * @param bonusBlock the generic that extend bonus passed as parameter.
     */
    void doHit(B bonusBlock);

}

