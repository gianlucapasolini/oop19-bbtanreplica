package model.bonusblock;

import element.Point2D;

/**
 * Interface that represent the logic bonus.
 *
 */
public interface BonusBlock {

    /**
     * Set the position of the bonus.
     *
     * @param newPosition the new position.
     */
    void setPosition(Point2D newPosition);

    /**
     * @return the position of the ball.
     */
    Point2D getPosition();

    /**
     * @return the radius of the ball.
     */
    double getRadius();

    /**
     * @return if the bonus is to be deleted after being hit once.
     */
    boolean isInstatDelete();

    /**
     * @return the string containing the path of the image.
     */
    String getImg();

    /**
     * @return whether or not the bonus has been hitted.
     */
    boolean isHitted();

    /**
     * When the bonus is hit.
     *
     * @return if the bonus is deleted after being hit once or not.
     */
    boolean hit();


}


