package model.collision;

import element.Point2D;
import model.ball.Ball;
import java.util.Optional;

/**
 * Interface to check if there is a collision.
 */
interface Collision {

    /**
     * Method to check if there is any collision with blocks or block.
     *
     * @param ball current ball
     * @param correctPosition correct position of ball
     * @return empty if there isn't any collision or Optional<CollisionDetected> if there is one
     */
    Optional<CollisionDetected> checkCollision(Ball ball, Point2D correctPosition);
}
