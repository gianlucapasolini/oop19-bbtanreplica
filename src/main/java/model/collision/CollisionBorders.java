package model.collision;

import element.Point2D;
import model.ball.Ball;

/**
 * Interface to manage the collisions with borders.
 */
interface CollisionBorders extends Collision {

    /**
     * Relocate the ball if is out of bounds.
     *
     * @param ball current ball
     * @return the new position (where was the collision) or the his position
     */
    Point2D relocateBall(Ball ball);

    /**
     * Method to check if a ball in Field.
     * @param ball current ball
     * @return return true or false if the ball is or not in field
     */
    boolean checkBallInField(Ball ball);
}
