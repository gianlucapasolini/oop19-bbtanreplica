package model.collision;

import javafx.scene.shape.Shape;
import model.ball.Ball;
import java.util.List;
import java.util.Optional;

/**
 * Interface to checking the collisions.
 */
public interface CollisionManager {
    /**
     * Method to check if there is any collision.
     *
     * @param ball the ball that i need to check
     * @return CollisionDetected with different cases:
     * <ul>
     *     <li>Empty if there isn't any collision</li>
     *     <li>If the collision is present there are 4 types of return CollisionDetectedImpl
     *         <ul>
     *             <li>BONUS, List of Shape contains the blocks and Vector2D and Point 2D are empty.</li>
     *             <li>BLOCKS, List of Shape contains the blocks, Vector2D that contain the new direction, Point 2D that
     *              contain the center of the ball where was the collision.</li>
     *             <li>BOTTOM BORDER, List of Shape empty, Vector2D empty, Point 2D that contain the center of the ball
     *              where was the collision.</li>
     *             <li>OTHER BORDERS, List of Shape empty, Vector2D that contain the new direction, Point 2D that contain
     *              the center of the ball where was the collision.</li>
     *         </ul>
     *     </li>
     * </ul>
     */
    Optional<CollisionDetected> checkCollision(Ball ball);

    /**
     * Method to Update all internal Blocks.
     *
     * @param blocks all the current blocks on the field
     */
    void updateBlocks(List<Shape> blocks);
}
