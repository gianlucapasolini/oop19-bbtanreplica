package model.collision;

import javafx.scene.shape.Shape;
import element.Point2D;
import element.Vector2D;
import java.util.List;
import java.util.Optional;

/**
 * This is the interface that allows me to create a class
 * that allows me to return multiple parameter from one method.
 */
public interface CollisionDetected {

    /**
     * Method to return all Blocks that have had a collision.
     *
     * @return an Optional immutableList of blocks
     */
    Optional<List<Shape>> getBlocks();

    /**
     * Method to return the new direction.
     *
     * @return an Optional for new direction
     */
    Optional<Vector2D> getNewDirection();

    /**
     * Method to return the new position of ball.
     *
     * @return an Optional for new center position
     */
    Optional<Point2D> getNewCenterPosition();
}
