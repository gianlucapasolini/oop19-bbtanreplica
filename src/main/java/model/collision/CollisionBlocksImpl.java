package model.collision;

import element.Point2D;
import element.Vector2D;
import element.Vector2DImpl;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.util.Pair;
import model.ball.Ball;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Class to implement all collision with blocks.
 */
final class CollisionBlocksImpl extends DefaultReallocation implements CollisionBlocks {
    private static final double PRECISION = .0001;
    private List<Shape> nodes;
    private Circle currBall;

    /**
     * Constructor of Blocks collision.
     *
     * @param blocks all current blocks
     */
    CollisionBlocksImpl(final List<Shape> blocks) {
        this.updateBlocks(blocks);
    }

    /**
     * Method to update all current Blocks.
     *
     * @param blocks all new current blocks
     */
    @Override
    public void updateBlocks(final List<Shape> blocks) {
        if (blocks != null) {
            nodes = new ArrayList<>(blocks);
        } else {
            nodes = Collections.emptyList();
        }
    }

    @Override
    public synchronized Optional<CollisionDetected> checkCollision(final Ball ball, final Point2D correctPosition) {
        this.setCurrBall(ball, correctPosition);
        final List<Shape> listNearShape = checkPresentNearShapes();
        if (!listNearShape.isEmpty()) {
            final var blockWithCollision = checkShapeIntersection(listNearShape);
            if (blockWithCollision.isPresent()) {
                if (blockWithCollision.get().getKey().isPresent()) {
                    System.out.println("Collision with at least one Block!!!!!");
                    final var penetration = getPenetration(blockWithCollision.get().getKey().get().getValue());
                    return Optional.of(new CollisionDetectedImpl(blockWithCollision.get().getValue(),
                            getNewDirectionOfBall(penetration, ball.getDirection()), getNewPositionOfBall(penetration,
                            correctPosition, ball.getDirection())));
                } else {
                    System.out.println("Collision with Bonus!!!");
                    return Optional.of(new CollisionDetectedImpl(blockWithCollision.get().getValue()));
                }
            } else {
                return Optional.empty();
            }
        } else {
            return Optional.empty();
        }
    }

    private void setCurrBall(final Ball ball, final Point2D correctPosition) {
        currBall = new Circle(correctPosition.getX(), correctPosition.getY(), ball.getRadius());
    }

    //method to boost performance
    private List<Shape> checkPresentNearShapes() {
        return nodes.stream()
                .filter(p -> Math.abs(p.getBoundsInLocal().getCenterX() - currBall.getCenterX()) < (currBall.getRadius() + p.getBoundsInLocal().getWidth()))
                .filter(p -> Math.abs(p.getBoundsInLocal().getCenterY() - currBall.getCenterY()) < (currBall.getRadius() + p.getBoundsInLocal().getHeight()))
                .collect(Collectors.toList());
    }

    /**
     * Method to check all shape intersection, also more than one.
     *
     * @return an optional of pair of the block hit and the intersection with the ball, and all list of hit blocks.
     */
    private Optional<Pair<Optional<Pair<Shape, Shape>>, List<Shape>>> checkShapeIntersection(final List<Shape> nearBlocks) {
        if (nearBlocks == null || nearBlocks.isEmpty()) {
            return Optional.empty();
        }
        final var collisions = checkBallIntersectWithShape(nearBlocks);
        if (collisions.getValue().isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(collisions);
        }
    }

    /**
     * Method to check all shape intersection, also more than one.
     *
     * @return a Pair of the block hit and the intersection with the ball (only if is a collision WIth Rectangle)
     *                and all list of hit blocks.
     */
    private Pair<Optional<Pair<Shape, Shape>>, List<Shape>> checkBallIntersectWithShape(final List<Shape> nearBlocks) {
        Pair<Optional<Pair<Shape, Shape>>, List<Shape>> allCollisionWithExplicitFirst = new Pair<>(Optional.empty(),
                new ArrayList<>());
        boolean isFirst = true;
        for (final Shape staticBloc : nearBlocks) {
            if (staticBloc != null) {
                final Shape intersect = Shape.intersect(currBall, staticBloc);
                if (intersect.getBoundsInLocal().getWidth() != -1) {
                    if (isFirst && staticBloc instanceof Rectangle) {
                        isFirst = false;
                        allCollisionWithExplicitFirst = new Pair<>(Optional.of(new Pair<>(staticBloc, intersect)),
                                allCollisionWithExplicitFirst.getValue());
                    }
                    allCollisionWithExplicitFirst.getValue().add(staticBloc);
                }
            }
        }
        return allCollisionWithExplicitFirst;
    }

    private Vector2D getPenetration(final Shape intersection) {
        final var penetration = intersection.getBoundsInLocal();
        if (Math.abs(penetration.getCenterX() - currBall.getCenterX()) < PRECISION) {
            System.out.println("Collision in Y Axis");
            System.out.println("i'm inside of  " + penetration.getHeight() + " in Y");
            return new Vector2DImpl(0, penetration.getHeight());
        } else if (Math.abs(penetration.getCenterY() - currBall.getCenterY()) < PRECISION) {
            System.out.println("Collision in X Axis");
            System.out.println("i'm inside of " + penetration.getWidth() + " in X");
            return new Vector2DImpl(penetration.getWidth(), 0);
        }
        System.out.println("angle collision");
        System.out.println("i'm inside of " + penetration.getWidth() + " in X and "  + penetration.getHeight() + " in Y");
        return new Vector2DImpl(penetration.getWidth(), penetration.getHeight());
    }
}
