package model.collision;

import javafx.scene.shape.Shape;
import model.ball.Ball;
import element.Point2D;
import element.Point2DImpl;
import java.util.List;
import java.util.Optional;

/**
 * Class that implement the CollisionManager and manage all type of collisions.
 */
public final class CollisionManagerImpl implements CollisionManager {
    private final CollisionBlocks blocksCollision;
    private final CollisionBorders bordersCollision;

    /**
     *Default empty Constructor.
     */
    public CollisionManagerImpl() {
        this(null, 0, 0, 0, 0);
    }

    /**
     * Constructor of CollisionManagerImpl with List of Shape and 2 opposite corner.
     *
     * @param blocks all the current blocks
     * @param topLeft position of top left corner
     * @param bottomRight position of bottom right corner
     */
    public CollisionManagerImpl(final List<Shape> blocks, final Point2D topLeft, final Point2D bottomRight) {
        bordersCollision = new CollisionBordersImpl(topLeft, bottomRight);
        blocksCollision = new CollisionBlocksImpl(blocks);
    }

    /**
     * Constructor of CollisionManagerImpl with List of Shape and the 4 different location (top, left, bottom, right).
     *
     * @param blocks all the current blocks
     * @param top the current position of top of the filed
     * @param left the current position of left of the filed
     * @param bottom the current position of bottom of the filed
     * @param right the current position of right of the filed
     */
    public CollisionManagerImpl(final List<Shape> blocks, final double top, final double left, final double bottom,
                                final double right) {
        this(blocks, new Point2DImpl(top, left), new Point2DImpl(bottom, right));
    }

    @Override
    public void updateBlocks(final List<Shape> blocks) {
        this.blocksCollision.updateBlocks(blocks);
    }

    @Override
    public Optional<CollisionDetected> checkCollision(final Ball ball) {
        //check only if the ball is block
        if (ball.getSpeed() == 0) {
            return Optional.empty();
        }
        Point2D ballCorrectPosition = ball.getPosition();
        boolean outBound = false;
        if (!bordersCollision.checkBallInField(ball)) {
            //reallocate the ball that is out of bound
            ballCorrectPosition = bordersCollision.relocateBall(ball);
            outBound = true;
        }
        //check also if there is any collision with all current blocks
        final var collision = blocksCollision.checkCollision(ball, ballCorrectPosition);
        if (collision.isEmpty()) {
            //check if the ball was out of bounds
            if (outBound) {
                return bordersCollision.checkCollision(ball, ballCorrectPosition);
            }
            return Optional.empty();
        }
        return collision;
    }
}
