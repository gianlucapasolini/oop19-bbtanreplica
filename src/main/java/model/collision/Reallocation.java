package model.collision;

import element.Point2D;
import element.Vector2D;

/**
 * Interface that represents the reallocation of the ball (position and direction).
 */
interface Reallocation {
    /**
     * Method to calculate the new position of ball, after a collision, with specific penetration.
     *
     * @param penetration Vector of the penetration of ball in something
     * @param ballPosition the current ball position
     * @param ballDirection the current ball direction
     * @return the new position of the ball
     */
    Point2D getNewPositionOfBall(Vector2D penetration, Point2D ballPosition, Vector2D ballDirection);

    /**
     * Method to calculate the new direction of ball, after a collision, with specific penetration.
     *
     * @param penetration Vector of the penetration of ball in something
     * @param ballDirection the current ball direction
     * @return the new direction of the ball
     */
    Vector2D getNewDirectionOfBall(Vector2D penetration, Vector2D ballDirection);
}
