package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.tinylog.Logger;

/**
 * This class represent the Main class of the JavaFX-based application.
 */
public final class Main extends Application {
    /**
     * default width.
     */
    private static final int WIDTH = 350;
    /**
     * default height.
     */
    private static final int HEIGHT = 600;

    @Override
    public void start(final Stage stage) throws Exception {
        //var variableDimension = getRightDimension();
        final Parent root = FXMLLoader.load(ClassLoader.getSystemResource("layouts/main.fxml"));
        final Scene scene = new Scene(root, WIDTH, HEIGHT);

        // Stage configuration.
        stage.setTitle("BBTAN replica");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();

        stage.setOnCloseRequest(event -> {
            Logger.info("Stage is closing");
            System.exit(0);
        });

        /*
        //Methods to future improve for dynamic change dimension

        //stage.setX(MouseInfo.getPointerInfo().getLocation().getX());
        //stage.setY(MouseInfo.getPointerInfo().getLocation().getY());
        Methods to future improve for dynamic change dimension

        stage.xProperty().addListener((obs, oldVal, newVal) -> {
            var dim2 = getRightDimension();
            System.out.println(stage.getWidth());
            System.out.println(scene.getWidth());
            System.out.println(stage.getWidth() - scene.getWidth());
            stage.setWidth(stage.getWidth() * dim2);
            stage.setHeight(stage.getHeight() * dim2);
        });
        */
    }

    /*
    Methods to future improve for dynamic change dimension

    private double getRightDimension() {
        //actual size screen
        final double currentWidth = MouseInfo.getPointerInfo().getDevice().getDisplayMode().getWidth(); //1920
        final double currentHeight = MouseInfo.getPointerInfo().getDevice().getDisplayMode().getHeight(); //1080
        //pref width in FHD
        final double fixAppWidth = 350;
        final double fixAppHeight = 600;
        //ratio between actual size screen and pref width in FHD
        final double ratioAppW = currentWidth / fixAppWidth;
        final double ratioAppH = currentHeight / fixAppHeight;
        //min between two ratio divide for (height FHD/ pref Height in FHD) (1080/600) = 1.8
        return Math.min(ratioAppW, ratioAppH) / 1.8;
    }*/

    /**
     * 
     * @param args unused
     */
    public static void main(final String[] args) {
        launch();
    }

}
