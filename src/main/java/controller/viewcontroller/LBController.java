package controller.viewcontroller;

import model.player.Player;
import model.player.PlayersCollection;
import model.player.PlayersCollectionImpl;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.json.simple.parser.ParseException;
import org.tinylog.Logger;
import utility.ImageEffects;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * This class defines a controller for the leaderboard.fxml, managing its events.
 */
public final class LBController implements Initializable {

    @FXML
    private ImageView restartButton;

    @FXML
    private BorderPane leaderPane;

    @FXML
    private Pane mainPageContent;

    private int callingPage;

    /**
     * This method is called to turn back to the game over page.
     * @throws IOException Throws IOException if any error occurred.
     */
    @FXML
    public void turnBack() throws IOException {
        Pane pane;

        if (callingPage == 1) {
            pane = FXMLLoader.load(ClassLoader.getSystemResource("layouts/endGamePage.fxml"));
        } else {
            pane = FXMLLoader.load(ClassLoader.getSystemResource("layouts/main.fxml"));
        }
        mainPageContent.getChildren().setAll(pane);
        Logger.info("PAGE CHANGING: Changed to previous page from game over page");
    }

    /**
     * This method is used to set data variabile for turning back to different pages.
     * If 1 is passed means that when turnBack() in fired is turned back to the game over page.
     * @param data Data to be passed as index turning back pages.
     */
    public void init(final int data) {
        this.callingPage = data;
    }

    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        restartButton.setOnMouseEntered(ImageEffects.imgMouseOver());
        restartButton.setOnMouseExited(ImageEffects.imgMouseExit());
        Logger.info("LOADING LEADERBOARD DATA...");
        try {
            loadPlayers();
            Logger.info("LOADED LEADERBOARD DATA SUCCESFULLY");
        } catch (IOException | ParseException  e) {
            Logger.error("ERROR WHILE LOADING LEADERBOARD DATA IN LEADERBOARD PAGE");
        }
    }

    /**
     * This method is called to load players for the leaderboard show.
     * @throws IOException Throws IOException if any error occurred.
     * @throws ParseException Throws ParseException if any error occurred.
     */
    private void loadPlayers() throws IOException, ParseException {
        final PlayersCollection list = PlayersCollectionImpl.getCollectionFromDisk();
        if (list.getPlayersInOrder().isEmpty()) {
            return;
        }
        this.callingPage = 0;
        int cont = 1;
        String styleClass = "";
        final VBox boxLeft = new VBox();
        final VBox boxRight = new VBox();

        for (final Player p : list.getPlayersInOrder().get()) {
            styleClass = "";
            if (cont == 1) {
                styleClass = "gold_text";
            } else {
                if (cont == 2) {
                    styleClass = "silver_text";
                } else {
                    if (cont == 3) {
                        styleClass = "bronze_text";
                    }
                }
            }

            final Label label1 = new Label(p.getName());
            label1.getStyleClass().add("scrollpane-text");
            label1.getStyleClass().add(styleClass);
            boxLeft.getChildren().add(label1);
            leaderPane.setLeft(boxLeft);

            final Label label2 = new Label(String.valueOf(p.getBestScore()));
            label2.getStyleClass().add("scrollpane-text");
            label2.getStyleClass().add(styleClass);
            boxRight.getChildren().add(label2);
            leaderPane.setRight(boxRight);
            cont++;
        }
    }
}
