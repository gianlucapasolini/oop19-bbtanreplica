package controller.viewcontroller;

import javafx.animation.Animation;
import javafx.animation.RotateTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import model.player.PlayersCollection;
import model.player.PlayersCollectionImpl;
import org.json.simple.parser.ParseException;
import utility.BallColor;

import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

/**
 * This class defines a controller for the fortuneWheelPage.fxml, managing its events.
 */
public final class FortuneWheelController implements Initializable {

    private static final int PRICE = 1;
    private static final int ROTATION_DEGREE = 720;
    private static final int ROTATION_ADJ = 90;

    private boolean running;

    @FXML
    private ImageView imgWheel;

    @FXML
    private Label lblColor;

    @FXML
    private Pane mainPageContent;

    @FXML
    private Label lblUpState;

    private PlayersCollection listPlayers;

    private Random random;

    /**
     * This method is used to rotate the fortune wheel.
     */
    @FXML
    public void rotate() {
        if (running) {
            return;
        }

        if (listPlayers.getCurrentPlayer().isEmpty()) {
            throw new IllegalStateException();
        }
        if (!listPlayers.getCurrentPlayer().get().hasEnoughMoney(PRICE)) {
            lblUpState.setText("Price: " + PRICE);
            lblColor.setText("Not enough money");
            return;
        }

        running = true;

        final int casuale = random.nextInt(13);
        final BallColor color = BallColor.getColorByIndex(casuale);
        final String name = color.getName();
        final int degree = color.getDegreeValue();
        final RotateTransition rotateTransition = new RotateTransition(Duration.seconds(3), this.imgWheel);
        rotateTransition.setFromAngle(0);
        rotateTransition.setToAngle(ROTATION_DEGREE + degree - ROTATION_ADJ);
        rotateTransition.setAutoReverse(false);
        rotateTransition.setCycleCount(1);

        if (rotateTransition.getStatus() == Animation.Status.RUNNING) {
            rotateTransition.pause();
        } else {
            rotateTransition.play();
        }

        final var ev = rotateTransition.onFinishedProperty();

        ev.setValue(actionEvent -> {
            lblColor.setText("Color: " + name);
            try {
                if (listPlayers.getCurrentPlayer().isEmpty()) {
                    throw new IllegalStateException();
                }
                listPlayers = PlayersCollectionImpl.getCollectionFromDisk();
                listPlayers.getCurrentPlayer().get().addNewColor(color);
                listPlayers.getCurrentPlayer().get().useMoney(PRICE);
                listPlayers.update();
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }
            running = false;
        });
    }

    /**
     * This method is used to turn back to the game over page.
     * @throws IOException Throws IOException if any error occurred.
     */
    @FXML
    public void turnBack() throws IOException {
        final Pane pane = FXMLLoader.load(ClassLoader.getSystemResource("layouts/endGamePage.fxml"));
        mainPageContent.getChildren().setAll(pane);
    }

    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        random = new Random();
        this.running = false;
        lblUpState.setText("Spin the fortune wheel");
        try {
            listPlayers = PlayersCollectionImpl.getCollectionFromDisk();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }
}
