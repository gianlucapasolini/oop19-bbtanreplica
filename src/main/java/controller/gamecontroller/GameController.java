package controller.gamecontroller;

import javafx.scene.shape.Shape;

import java.util.List;

/**
 * Interface used for Game
 */
public interface GameController {

    /**
     * @return value of maxBlockOnLine
     * this method is a getter of maxBlockOnLine.
     */
    int getMaxBlockOnLine();

    /**
     * @return value of differenceBonusNormal
     * this method is a getter of differenceBonusNormal.
     */
    double getDifferenceBonusNormal();

    /**
     * @return value of headerY
     * this method is a getter of headerY.
     */
    double getHeaderY();

    /**
     * @param bonusProbability probability of Bonus
     * this method is a setter of bonusProbability.
     */
    void setBonusProbability(int bonusProbability);

    /**
     * @return true if yes, false if no
     * this method tells us if it's time to create a new block or not.
     */
    boolean setNormalBlock();

    /**
     * @return true if yes, false if no
     * this method tells us if it's time to create a new bonus block or not.
     */
    boolean setBonusBlock();

    /**
     * @param x position x of the normal block
     * @param y position y of the normal block
     * @param amount how much block got hit
     * @return new normal block
     * allows you to create a new normal block.
     */
    Shape newNormalBlock(double x, double y, int amount);

    /**
     * @param x position x of the bonus block
     * @param y position y of the bonus block
     * @return new bonus block
     * allows you to create a new bonus block.
     */
    Shape newBonusBlock(double x, double y);

    /**
     * @param value block value of line
     * @return list of block
     * create new line of block (normal and bonus).
     */
    List<Shape> newBlockLine(int value);

    /**
     * @return true if lose, false otherwise
     * control if the match is finished.
     */
    boolean checkLose();

    /**
     * this method calls the method of maincontroller
     * and add coin for the player.
     */
    void addCoin();

    /**
     * this method calls the method of maincontroller
     * and add ball for the player.
     */
    void addBall();

}
