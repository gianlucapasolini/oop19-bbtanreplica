package controller.mouseactions;

import controller.maincontroller.MainController;
import element.Point2D;
import element.Point2DImpl;
import element.Vector2D;
import element.Vector2DImpl;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import view.ArrowImplVector;

public class MouseMovement {

    private static final double ANGLEADJUSTMENT = -180;

    private final MainController mainController;
    private final Pane pane;
    private final ArrowImplVector arrow;
    private Point2D start;
    private Point2D end;
    private Vector2D vector;
    private boolean launcheable;

    public MouseMovement(final Pane pane, final MainController c) {
        this.mainController = c;
        this.arrow = new ArrowImplVector();
        this.pane = pane;
        this.pane.getChildren().add(this.arrow);
    }

    /**
     * Add all the EventHandler to the panel passed in Input.
     */
    public void addAllEvent() {
        addMouseEventPress();
        addMouseEventDragged();
        addMouseEventRelease();
    }

    private void addMouseEventPress() {
        this.pane.addEventHandler(MouseEvent.MOUSE_PRESSED, e -> {
            launcheable = this.mainController.getStartPosition().isPresent();
            if (launcheable) {
                this.vector = new Vector2DImpl(0, 0);
                this.arrow.setStart(this.mainController.getStartPosition().get());
                this.start = new Point2DImpl(e.getX(), e.getY());
            }
        });
    }

    private void addMouseEventDragged() {
        this.pane.addEventHandler(MouseEvent.MOUSE_DRAGGED, e -> {
            if (launcheable) {
                this.arrow.setVisible(true);
                this.end = new Point2DImpl(e.getX(), e.getY());
                vector = start.subtraction(end);
                if (!vector.isNullVector()) {
                    this.arrow.setEnd(vector);
                    if (mainController.getMinusAngleDegrees() < vector.getDegreesAngle() && ANGLEADJUSTMENT + (-(mainController.getMinusAngleDegrees())) > vector.getDegreesAngle()) {
                        this.arrow.setColor(Color.WHITE);
                    } else {
                        this.arrow.setColor(Color.RED);
                    }
                }
            }
        });
    }

    private void addMouseEventRelease() {
        this.pane.addEventHandler(MouseEvent.MOUSE_RELEASED, mouseEvent -> {
            if (launcheable && !vector.isNullVector()) {
                this.arrow.setVisible(false);
                mainController.setPositionLaunch(vector);
            }
        });
    }
}
