package controller.maincontroller;

import element.Point2D;
import element.Vector2D;
import javafx.scene.shape.Shape;
import model.ball.Ball;
import org.json.simple.parser.ParseException;
import utility.BallColor;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * This interface model the core of the game: the Main Controller.
 */
public interface MainController {

    /**
     * @param blocks Identify the hitted blocks to be notified.
     */
    void notifyHittedBlocks(List<Shape> blocks);

    /**
     * This method increment money earned in game.
     */
    void incrementMoney();

    /**
     * Add one extra ball for the next launch.
     */
    void addBall();

    /**
     * @param vector Set the launch vector.
     */
    void setPositionLaunch(Vector2D vector);

    /**
     * @return Return the balls set that can be launched.
     */
    Set<Ball> getBalls();

    /**
     * @return Return the blocks list currently on the game.
     */
    List<Shape> getBlocks();

    /**
     * @return Return Point2D that represent the start point of the launch
     * to draw trajectory and perform launch calculating launch vector.
     * Return an empty optional if launch is running.
     */
    Optional<Point2D> getStartPosition();

    /**
     * @return Return the amount of money earned.
     */
    int getEarnedMoney();

    /**
     * Set the game over state, stopping the game main thread.
     */
    void gameOver();

    /**
     * This method pause the game.
     */
    void pauseGame();

    /**
     * This method resume the game.
     */
    void restartGame();

    /**
     * @return Return the best score of the current player.
     * @throws IOException    Throws IOException if any error reading the players collection occurs.
     * @throws ParseException Throws ParseException if any error parsing the players collection occurs.
     */
    int getPlayerBestScore() throws IOException, ParseException;

    /**
     * @return Return the current color used by the player.
     */
    BallColor getColor();

    /**
     * @return Return the current radius used by the user.
     */
    int getRadius();

    /**
     * @return Return the start position when the first launch is performed.
     */
    Point2D getBallsStartPosition();

    /**
     * @return Return the minus angle degree of launch vector.
     */
    double getMinusAngleDegrees();

    /**
     * This method perform the launch.
     */
    void launch();

    /**
     * This method is called to draw and perform new game cycle.
     */
    void newGameCycle();
}
