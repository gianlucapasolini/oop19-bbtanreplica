package controller.ballscontroller.time;

/**
 * 
 * Interface of Timer.
 *
 */
public interface Timer {

    /**
     * reset the timer to the current time.
     */
    void reset();

    /**
     * @return the time interval between the previous call and this one
     */
    double getInterval();
}
