package controller.ballscontroller.time;

/**
 * The Interface to get the elapsed time.
 *
 */
@FunctionalInterface
public interface Time {

    /**
     * @param interval    the abstract interval
     * @param elapsedTime the real interval
     * @return the selected type of interval
     */
    double getElapsedTime(long interval, long elapsedTime);
}
