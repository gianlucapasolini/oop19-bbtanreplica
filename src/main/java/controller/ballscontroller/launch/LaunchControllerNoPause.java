package controller.ballscontroller.launch;

import element.Vector2D;

/**
 * 
 * Class for a LaunchController without pause.
 *
 */
public class LaunchControllerNoPause implements LaunchController {

    private final LaunchController thread;

    /**
     * 
     * @param launchController the launch controller with pause
     */
    public LaunchControllerNoPause(final LaunchController launchController) {
        this.thread = launchController;
    }

    /**
     *
     * @param vector2D the vector to check
     * @return true if @param vector2D is a valid angle
     */
    @Override
    public boolean isValidVector(final Vector2D vector2D) {
        return this.thread.isValidVector(vector2D);
    }

    /**
     *
     * @param direction the direction of the launch
     */
    @Override
    public void setVector(final Vector2D direction) {
        this.thread.setVector(direction);
    }

    /**
     * launch the balls.
     */
    @Override
    public void launch() {
        this.thread.launch();
    }

    /**
     * not supported operation.
     */
    @Override
    public final void restart() {
        throw new UnsupportedOperationException();
    }

    /**
     * not supported operation.
     */
    @Override
    public void pause() {
        throw new UnsupportedOperationException();
    }

    /**
     * end the game for the launch.
     */
    @Override
    public void endGame() {
        this.thread.endGame();
    }

    /**
     *
     * @return true if the launch is paused
     */
    @Override
    public boolean isPause() {
        return this.thread.isPause();
    }

    /**
     *
     * @return true if the launch is over
     */
    @Override
    public boolean isOver() {
        return this.thread.isOver();
    }

    /**
     *
     * @return true if the game is end
     */
    @Override
    public boolean isEndGame() {
        return this.thread.isEndGame();
    }

}
