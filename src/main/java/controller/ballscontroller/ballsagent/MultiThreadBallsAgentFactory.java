package controller.ballscontroller.ballsagent;

import controller.ballscontroller.AbstractMoveThread;

import controller.ballscontroller.Pause;
import controller.ballscontroller.AbstractPauseThread;
import controller.ballscontroller.time.Time;
import controller.ballscontroller.time.TimeConverter;
import model.ball.Ball;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Factory that create BallsAgent that create a thread for each balls moved.
 *
 */
public class MultiThreadBallsAgentFactory extends AbstractBallsAgentFactory {

    /**
     *
     * @param timeConverter the unit to converter
     * @param timeInterval  the interval between the ball
     * @param calculateTime the function to calculate the time
     * @return a BallsAgentMovement with multithread
     */
    @Override
    protected BallsAgentMovement ballsAgentMovement(final TimeConverter timeConverter, final long timeInterval, final Time calculateTime) {
        return new BallsAgentMovement() {

            private final Map<Ball, AbstractMoveThread> mapping = new HashMap<>();

            @Override
            public synchronized void restart() {
                if (!this.isOver()) {
                    for (final Pause pause : mapping.values()) {
                        pause.restart();
                    }
                }
            }

            @Override
            public synchronized void pause() {
                if (!this.isOver()) {
                    for (final Pause pause : mapping.values()) {
                        pause.pause();
                    }
                }
            }

            @Override
            public synchronized void endGame() {
                mapping.values().forEach(AbstractPauseThread::endGame);
            }

            @Override
            public synchronized boolean isPause() {
                return !mapping.isEmpty() && mapping.values().stream().allMatch(AbstractPauseThread::isPause);
            }

            @Override
            public synchronized boolean isOver() {
                return !mapping.isEmpty() && mapping.values().stream().allMatch(AbstractPauseThread::isOver);
            }

            @Override
            public synchronized boolean isEndGame() {
                return !mapping.isEmpty() && mapping.values().stream().allMatch(AbstractPauseThread::isEndGame);
            }

            @Override
            public synchronized void move(final Ball ball) {
                if (!(this.isEndGame() || this.isPause())) {
                    if (!mapping.containsKey(ball)) {
                        mapping.put(ball, new AbstractMoveThread(calculateTime, timeConverter, timeInterval) {

                            @Override
                            protected void move(final double interval) {
                                ball.moveByTime(interval);
                            }

                            @Override
                            protected boolean conditionOver() {
                                return ball.isStationary();
                            }
                        });
                    }
                    mapping.get(ball).start();
                }
            }

            @Override
            public AbstractMoveThread getBallThread(final Ball ball) {
                return mapping.get(ball);
            }

            @Override
            public Set<Ball> getBalls() {
                return this.mapping.keySet();
            }
        };
    }
}
