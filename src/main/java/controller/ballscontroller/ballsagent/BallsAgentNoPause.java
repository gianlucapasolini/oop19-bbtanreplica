package controller.ballscontroller.ballsagent;

import element.Point2D;

import model.ball.Ball;

import java.util.Optional;

/**
 * A class that create a ballsAgent without pause from one with pause.
 */

public class BallsAgentNoPause implements BallsAgent {

    private final BallsAgent ballsAgent;

    /**
     * @param ballsAgent the BallsAgent with pause.
     */
    public BallsAgentNoPause(final BallsAgent ballsAgent) {
        this.ballsAgent = ballsAgent;
    }

    /**
     * @throws UnsupportedOperationException always
     */
    @Override
    public void restart() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * @throws UnsupportedOperationException always
     */
    @Override
    public void pause() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * end the game.
     */
    @Override
    public void endGame() {
        this.ballsAgent.endGame();
    }

    /**
     * move the ball.
     */
    @Override
    public final void move(final Ball ball) {
        ballsAgent.move(ball);
    }

    /**
     * @return false
     */
    @Override
    public boolean isPause() {
        return false;
    }

    /**
     * @return true if the ballsAgent is over
     */
    @Override
    public boolean isOver() {
        return ballsAgent.isOver();
    }

    /**
     * @return true if game is end
     */
    @Override
    public boolean isEndGame() {
        return this.ballsAgent.isEndGame();
    }

    /**
     * set the end point.
     */
    @Override
    public void setEndPoint(final Point2D endPoint) {
        ballsAgent.setEndPoint(endPoint);
    }

    /**
     * @return the start point.
     */
    @Override
    public final Point2D getStartPoint() {
        return ballsAgent.getStartPoint();
    }

    /**
     * @return the end point if is present.
     */
    @Override
    public Optional<Point2D> getEndPoint() {
        return ballsAgent.getEndPoint();
    }

    /**
     * group the @param ball with the other.
     */
    @Override
    public void groupBall(final Ball ball) {
        ballsAgent.groupBall(ball);
    }

    /**
     * @return true if the ball are stationary.
     */
    @Override
    public boolean areBallsStationary() {
        return ballsAgent.areBallsStationary();
    }

    /**
     * @return true if the ball are grouped.
     */
    @Override
    public boolean areBallsGrouped() {
        return this.ballsAgent.areBallsGrouped();
    }
}
