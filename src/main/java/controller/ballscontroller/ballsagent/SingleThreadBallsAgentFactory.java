package controller.ballscontroller.ballsagent;

import controller.ballscontroller.AbstractMoveThread;
import controller.ballscontroller.time.Time;
import controller.ballscontroller.time.TimeConverter;
import model.ball.Ball;

import java.util.HashSet;
import java.util.Set;

public class SingleThreadBallsAgentFactory extends AbstractBallsAgentFactory {


    private static class BallsAgentThread extends AbstractMoveThread implements BallsAgentMovement {

        private final Set<Ball> balls = new HashSet<>();


        BallsAgentThread(final TimeConverter timeConverter, final long timeInterval, final Time calculateTime) {
            super(calculateTime, timeConverter, timeInterval);
        }

        @Override
        protected void move(final double interval) {
            synchronized (balls) {
                balls.removeIf(Ball::isStationary);
                balls.forEach(ball -> ball.moveByTime(interval));
            }
        }

        @Override
        protected synchronized boolean conditionOver() {
            return balls.isEmpty() || balls.stream().allMatch(Ball::isStationary);
        }

        @Override
        public final void move(final Ball ball) {
            if (this.isOver()) {
                this.start();
            }
            synchronized (this) {
                this.balls.add(ball);
            }
        }

        @Override
        public AbstractMoveThread getBallThread(final Ball ball) {
            return this;
        }

        @Override
        public Set<Ball> getBalls() {
            return this.balls;
        }
    }

    /**
     *
     * @param timeConverter the unit to converter
     * @param timeInterval  the interval between the ball
     * @param calculateTime the function to calculate the time
     * @return a BallsAgentMovement with single new thread
     */
    @Override
    protected BallsAgentMovement ballsAgentMovement(final TimeConverter timeConverter, final long timeInterval, final Time calculateTime) {
        return new BallsAgentThread(timeConverter, timeInterval, calculateTime);
    }
}
