package utility;

public final class ViewChangeDimension {

    private final static Source<Double> view = new SourceImpl<>();

    private ViewChangeDimension() { }

    public static Source<Double> getViewChangeObject() {
        return view;
    }

    public static void notifyNewDimension(final double newDimension) {
        view.notifyObservers(newDimension);
    }

}
