package utility;

/**
 * 
 *
 * @param <T> the type of the value notify by the source.
 */
@FunctionalInterface
public interface Observer<T> {

    /**
     * @param source   the source of the call
     * @param argument the new object
     */
    void update(Source<? extends T> source, T argument);
}
