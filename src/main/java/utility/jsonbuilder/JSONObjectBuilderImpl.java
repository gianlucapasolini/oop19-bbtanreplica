package utility.jsonbuilder;

import org.json.simple.JSONObject;

/**
 * This class implements JSONObjectBuilder interface.
 */
public final class JSONObjectBuilderImpl implements JSONObjectBuilder {

    private final JSONObject jsonObject = new JSONObject();

    @SuppressWarnings("unchecked")
    @Override
    public JSONObjectBuilder put(final String name, final Object o) {
        jsonObject.put(name, o);
        return this;
    }

    @Override
    public JSONObject build() {
        return this.jsonObject;
    }
}
