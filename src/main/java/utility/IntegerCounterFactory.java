package utility;

/**
 * Factory for counter of integer.
 *
 */
public class IntegerCounterFactory extends AbstractCounterFactory<Integer> implements CounterFactory<Integer> {

    private static final int STANDARD_START = 0;
    private static final int STANDARD_STEP = 1;

    /**
     * Create a standard counter.
     */
    @Override
    public Counter<Integer> standardCounter() {
        return this.standardCounterFromValue(STANDARD_START);
    }

    /**
     * Create a standard counter from value.
     */
    @Override
    public Counter<Integer> standardCounterFromValue(final Integer startValue) {
        return this.counterFromValueWithStep(startValue, STANDARD_STEP);
    }

    /**
     * Create a standard counter with step.
     */
    @Override
    public Counter<Integer> counterWithStep(final Integer step) {
        return this.counterFromValueWithStep(STANDARD_START, step);
    }

    /**
     * Create a standard counter from value and with step .
     */
    @Override
    public Counter<Integer> counterFromValueWithStep(final Integer startValue, final Integer step) {
        return this.counterFromValueWithStep(startValue, step, Integer::sum);
    }
}
