package utility;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Factory to create Range.
 */
public class RangeFactoryImpl implements RangeFactory {

    private static final int STANDARD_START = 0;
    private static final int STANDARD_STEP = 1;

    /**
     * Create range with limit.
     *
     * @throws IllegalArgumentException if the parameters don't are compatible.
     */
    @Override
    public final Range standardRange(final int limit) {
        return this.rangeWithStepFrom(STANDARD_START, limit, STANDARD_STEP);
    }

    /**
     * Create range with limit and step.
     *
     * @throws IllegalArgumentException if the parameters don't are compatible.
     */
    @Override
    public final Range rangeWithStep(final int limit, final int step) {
        return this.rangeWithStepFrom(STANDARD_START, limit, step);
    }

    /**
     * Create range with start and limit.
     *
     * @throws IllegalArgumentException if the parameters don't are compatible.
     */
    @Override
    public final Range standardRangeFrom(final int start, final int limit) {
        return this.rangeWithStepFrom(start, limit, STANDARD_STEP);
    }

    /**
     * Create range with start, limit and step.
     *
     * @throws IllegalArgumentException if the parameters don't are compatible.
     */
    @Override
    public final Range rangeWithStepFrom(final int start, final int limit, final int step) {
        if (step != 0 && (limit - start) / step < 0) {
            throw new IllegalArgumentException();
        }
        return new Range() {
            @Override
            public int getStart() {
                return start;
            }

            @Override
            public int getLimit() {
                return limit;
            }

            @Override
            public int getStep() {
                return step;
            }

            @Override
            public Iterator<Integer> iterator() {
                return new Iterator<>() {

                    private int current = start;

                    @Override
                    public boolean hasNext() {
                        return this.current < limit;
                    }

                    @Override
                    public Integer next() {
                        if (hasNext()) {
                            return this.current++;
                        } else {
                            throw new NoSuchElementException("Range reached the end");
                        }
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException("Can't remove values from a Range");
                    }
                };
            }
        };
    }
}
