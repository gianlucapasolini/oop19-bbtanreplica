package utility;

import java.util.function.BinaryOperator;

/**
 * Factory for counter with generic.
 *
 * @param <T> value of the counter
 */
public abstract class AbstractCounterFactory<T extends Number & Comparable<T>> implements CounterFactory<T> {

    /**
     * @param startValue the start value of the counter
     * @param step       the value of the step of the counter
     * @param sum        the BinaryOperator between step and current value
     * @return a counter
     */
    protected Counter<T> counterFromValueWithStep(final T startValue, final T step, final BinaryOperator<T> sum) {
        return new Counter<>() {

            private T value = startValue;

            @Override
            public T getValue() {
                return this.value;
            }

            @Override
            public T getStep() {
                return step;
            }

            @Override
            public T getStart() {
                return startValue;
            }

            @Override
            public void increment() {
                this.value = sum.apply(value, step);
            }

            @Override
            public void reset() {
                this.value = startValue;
            }
        };
    }
}
