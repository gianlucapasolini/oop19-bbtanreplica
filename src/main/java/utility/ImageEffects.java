package utility;

import javafx.event.EventHandler;
import javafx.scene.effect.Glow;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public final class ImageEffects {

    private static final double EFFECTSVALUE = 0.2;
    private static final double STANDARDVALUE = 0.0;
    /**
     * This is a simple image effect change when mouse is over an image.
     */
    private static final EventHandler<MouseEvent> MOUSEOVER = mouseEvent -> {
        final ImageView img = (ImageView) mouseEvent.getSource();
        final Glow glow = new Glow(EFFECTSVALUE);
        img.setEffect(glow);
    };

    /**
     * This is a simple image effect change when mouse is exiting from an image.
     */
    private static final EventHandler<MouseEvent> MOUSEEXIT = mouseEvent -> {
        final ImageView img = (ImageView) mouseEvent.getSource();
        final Glow glow = new Glow(STANDARDVALUE);
        img.setEffect(glow);
    };

    private ImageEffects() {

    }

    public static EventHandler<MouseEvent> imgMouseOver() {
        return MOUSEOVER;
    }

    public static EventHandler<MouseEvent> imgMouseExit() {
        return MOUSEEXIT;
    }

    /**
     * Method to add mouse over and exit animation for image.
     * @param image to set the animation.
     */
    public static void addAnimationOnImage(final ImageView image) {
        image.setOnMouseEntered(ImageEffects.imgMouseOver());
        image.setOnMouseExited(ImageEffects.imgMouseExit());
    }
}
