package utility;

import java.util.Set;

/**
 * Interface useful for Observer pattern.
 *
 * @param <T> the type of the value of the source
 */
public interface Source<T> {

    /**
     * @param observer the observer to add
     * @throws NullPointerException if @param observer is null
     *                              <p>
     *                              add the observer
     *                              </p>
     */

    void addObserver(Observer<? super T> observer);

    /**
     * @param observer the observer to remove
     * @throws NullPointerException if @param observer is null
     *                              <p>
     *                              remove the observer
     *                              </p>
     */

    void removeObserver(Observer<? super T> observer);

    /**
     * @return all the Observer of the source
     */

    Set<Observer<? super T>> getObserversSet();


    /**
     * @param arg the change to notify
     *            <p>
     *            notify the change to all the observer
     *            </p>
     */

    void notifyObservers(T arg);
}
