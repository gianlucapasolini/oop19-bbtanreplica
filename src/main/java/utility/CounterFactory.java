package utility;

/**
 * 
 * Factory for the counter.
 *
 * @param <T> type of value of counter
 */
public interface CounterFactory<T extends Number & Comparable<T>> {

    /**
     * @return a Counter that start from 0 with step 1
     */
    Counter<T> standardCounter();

    /**
     * @param startValue the start value
     * @return a Counter that start from @param startValue with step 1
     */

    Counter<T> standardCounterFromValue(T startValue);

    /**
     * @param step the step of the increment
     * @return a Counter that start from 0 with @param step
     */

    Counter<T> counterWithStep(T step);

    /**
     * @param startValue the start value
     * @param step       he step of the increment
     * @return a Counter that start from @param startValue with @param step
     */

    Counter<T> counterFromValueWithStep(T startValue, T step);
}
