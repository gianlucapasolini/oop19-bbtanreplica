package utility;

import javafx.scene.paint.Color;

public enum BallColor {
    /**
     * Color red representation.
     */
    RED(0, 165, "Rosso", Color.RED),
    /**
     * Color blue representation.
     */
    BLUE(1, 315, "Blu", Color.BLUE),
    /**
     * Color light blue representation.
     */
    LIGHT_BLUE(2, 345, "Azzurro", Color.LIGHTBLUE),
    /**
     * Color green representation.
     */
    GREEN(3, 45, "Verde", Color.GREEN),
    /**
     * Color light green representation.
     */
    LIGHT_GREEN(4, 80, "Verde chiaro", Color.LIGHTGREEN),
    /**
     * Color aqua marine representation.
     */
    AQUA_MARINE(5, 15, "Verde acqua", Color.SEAGREEN),
    /**
     * Color dark purple representation.
     */
    DARK_PURPLE(6, 285, "Viola scuro", Color.PURPLE),
    /**
     * Color purple representation.
     */
    PURPLE(7, 255, "Viola", Color.MEDIUMPURPLE),
    /**
     * Color violet representation.
     */
    VIOLET(8, 220, "Violetto", Color.VIOLET),
    /**
     * Color fucsia representation.
     */
    FUCSIA(9, 195, "Fucsia", Color.FUCHSIA),
    /**
     * Color yellow representation.
     */
    YELLOW(10, 105, "Giallo", Color.YELLOW),
    /**
     * Color orange representation.
     */
    ORANGE(11, 135, "Arancione", Color.ORANGE),
    /**
     * Color white representation.
     */
    WHITE(12, 0, "Bianco", Color.WHITE);


    private final int index;
    private final int degree;
    private final String name;
    private final Color color;

    BallColor(final int index, final int degree, final String name, final Color color) {
        this.index = index;
        this.degree = degree;
        this.name = name;
        this.color = color;
    }

    public static BallColor getColorByIndex(final int index) {
        switch (index) {
            case 0:
                return RED;
            case 1:
                return BLUE;
            case 2:
                return LIGHT_BLUE;
            case 3:
                return GREEN;
            case 4:
                return LIGHT_GREEN;
            case 5:
                return AQUA_MARINE;
            case 6:
                return DARK_PURPLE;
            case 7:
                return PURPLE;
            case 8:
                return VIOLET;
            case 9:
                return FUCSIA;
            case 10:
                return YELLOW;
            case 11:
                return ORANGE;
            case 12:
                return WHITE;
            default:
                throw new IllegalStateException();
        }
    }

    public Color getColor() {
        return this.color;
    }

    public String getName() {
        return this.name;
    }

    public int getDegreeValue() {
        return this.degree;
    }

    public int getIndex() {
        return this.index;
    }

}
