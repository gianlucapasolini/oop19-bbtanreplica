package utility;

/**
 * Factory for the limitCounter.
 *
 */
public interface LimitCounterFactory {

    /**
     * Interface for the counter with observer.
     * 
     * @param <T> Parameter of the CounterLimit.
     */
    interface CounterWithEvents<T extends Number & Comparable<T>> extends Counter<T>, Source<T> {
    }

    /**
     * @param counter the counter to use
     * @param limit   the limit of the counter
     * @param <T>     the type of the counter
     * @return a Counter that is limited
     */
    <T extends Number & Comparable<T>> Counter<T> limitCounter(Counter<T> counter, T limit);

    /**
     * @param counter the counter to use
     * @param limit   the limit of the counter
     * @param <T>     the type of the counter
     * @return a Counter that is limited and that notify when is reach then reset
     */
    <T extends Number & Comparable<T>> CounterWithEvents<T> limitCounterNotifyAndReset(Counter<T> counter, T limit);

    /**
     * @param counter       the counter to use
     * @param limit         the limit of the counter
     * @param limitObserver the observer to add
     * @param <T>           the type of the counter
     * @return a Counter that is limited and that notify when is reach then reset
     */
    <T extends Number & Comparable<T>> CounterWithEvents<T> limitCounterNotifyAndReset(Counter<T> counter, T limit,
            Observer<T> limitObserver);
}
