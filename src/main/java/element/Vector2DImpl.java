package element;

/**
 * Class that represent the vector2D.
 *
 */
public class Vector2DImpl implements Vector2D {

    private final double xComponent;
    private final double yComponent;

    /**
     * 
     * @param xComponent x component of the vector
     * @param yComponent y component of the vector
     */
    public Vector2DImpl(final double xComponent, final double yComponent) {
        final int precision = 8;
        this.xComponent = Elements.round(xComponent, precision);
        this.yComponent = Elements.round(yComponent, precision);

    }

    private void nullVectorCheck() {
        if (this.isNullVector()) {
            throw new IllegalStateException();
        }
    }

    /**
     * @return the value of the x component of the vector
     */
    @Override
    public double getXComponent() {
        return this.xComponent;
    }

    /**
     * @return the value of the y component of the vector
     */
    @Override
    public double getYComponent() {
        return this.yComponent;
    }

    /**
     * @return the value in radiant of the angle of the vector
     * @throws IllegalStateException if the vector is arithmetic null
     */
    @Override
    public double getRadiansAngle() {
        this.nullVectorCheck();
        return Math.atan2(this.getYComponent(), this.getXComponent());
    }

    /**
     * @return the value in degrees of the angle of the vector
     * @throws IllegalStateException if the vector is arithmetic null
     */
    @Override
    public double getDegreesAngle() {
        return Math.toDegrees(this.getRadiansAngle());
    }

    /**
     * @return true if the vector is arithmetic null, else return false
     */
    @Override
    public boolean isNullVector() {
        return this.getXComponent() == 0 && this.getYComponent() == 0;
    }

    /**
     * @return the module of the vector
     */
    @Override
    public double getModulus() {
        return Math.sqrt(Math.pow(this.getXComponent(), 2) + Math.pow(this.getYComponent(), 2));
    }

    /**
     * @return a new vector with the same angle but with module 1
     * @throws ArithmeticException   if the vector is arithmetic null
     * @throws IllegalStateException if the vector is arithmetic null
     */
    @Override
    public Vector2D getNormalizedVector() {
        this.nullVectorCheck();
        final double modulus = this.getModulus();
        return new Vector2DImpl(this.getXComponent() / modulus, this.getYComponent() / modulus);
    }

    /**
     * @param scalar the scalar value of the multiplication
     * @return a new vector with module of the actual vector multiplied by scalar
     */
    @Override
    public Vector2D scalarMultiplication(final double scalar) {
        return new Vector2DImpl(this.getXComponent() * scalar, this.getYComponent() * scalar);
    }

    /**
     * @param vector2D the vector to compare
     * @return true if the normalized vectors are equals with precision 4
     * @throws IllegalStateException if either of them is arithmetic null
     */
    @Override
    public boolean hasSameNormalizedVector(final Vector2D vector2D) {
        final int precision = -2;
        return this.hasSameNormalizedVector(vector2D, precision);
    }

    /**
     * @param vector2D  the vector to compare
     * @param precision the precision of the comparison
     * @return true if the normalized vectors are equals with precision @param
     *         precision
     * @throws IllegalStateException if either of them is arithmetic null
     */
    @Override
    public boolean hasSameNormalizedVector(final Vector2D vector2D, final int precision) {
        if (this.isNullVector() || vector2D.isNullVector()) {
            throw new IllegalStateException();
        }
        final Vector2D v1 = this.getNormalizedVector();
        final Vector2D v2 = vector2D.getNormalizedVector();
        final double delta = Math.pow(10, precision);
        return Math.abs(v1.getXComponent() - v2.getXComponent()) <= delta
                && Math.abs(v1.getYComponent() - v2.getYComponent()) <= delta;

    }

    /**
     * @param vector2D the Vector value of the multiplication
     * @return a new vector resulting from multiplication between current Vector and
     *         another
     */
    @Override
    public Vector2D vectorMultiplication(final Vector2D vector2D) {
        return new Vector2DImpl(this.getXComponent() * vector2D.getXComponent(),
                this.getYComponent() * vector2D.getYComponent());
    }

    /**
     *
     * @return a string that represent the vector
     */
    @Override
    public String toString() {
        return "Vector2D ( x: " + this.xComponent + ", y :" + this.yComponent + " )";
    }

    /**
     *
     * @param o the other object
     * @return true if the two vector have the same component
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vector2DImpl)) {
            return false;
        }

        final Vector2DImpl vector2D = (Vector2DImpl) o;

        if (Double.compare(vector2D.getXComponent(), this.getXComponent()) != 0) {
            return false;
        }
        return Double.compare(vector2D.getYComponent(), this.getYComponent()) == 0;
    }

    /**
     *
     * @return a integer that represent the hash of the vector
     */
    @Override
    public int hashCode() {
        final int hash = 31;
        int result;
        long temp;
        temp = Double.doubleToLongBits(this.getXComponent());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(this.getYComponent());
        result = hash * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

}
