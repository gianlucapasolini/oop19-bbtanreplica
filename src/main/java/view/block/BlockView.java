package view.block;

import javafx.scene.text.Text;
import model.block.Block;

/**
 * Interface used for graphic block
 */
public interface BlockView {

    /**
     * @param control it's used to understand if we have a hit o a new line (different brighten)
     * this method permit to change the brightness color of block
     */
    void updateBrightnessColor(boolean control);

    /**
     * @return the Text of value block
     * getter of Text
     */
    Text getText();

    /**
     * @return the 'logic' block
     * getter of 'logic' block
     */
    Block getBlock();

    /**
     * @return true if the block has been deleted (value minor or equal to 0), false otherwise
     */
    boolean deletedBlock();

    /**
     * @param value amount of hit
     * this method is called when a block get hitted, he change color and block value
     */
    void isHitted(int value);

}
