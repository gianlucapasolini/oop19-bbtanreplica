package view.ball;

import element.Point2D;
import utility.BallColor;

/**
 * Factory to create ballView.
 *
 * @param <T> the view element that represent the ball.
 */
public interface BallViewFactory<T extends BallView> {

    /**
     * @param position the position of the ball
     * @param radius   the radius of the ball
     * @param color    the color of the ball
     * @return a Node that represents a Ball
     */

    T standardBalls(Point2D position, double radius, BallColor color);

}

