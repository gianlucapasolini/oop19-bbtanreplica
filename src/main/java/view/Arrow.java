package view;

import javafx.scene.paint.Color;

/**
 * @param <P> The type to pass in setStart.
 * @param <V> The type to pass in setEnd.
 */
public interface Arrow<P, V> {

    /**
     * Sets the end of the arrow.
     *
     * @param vector the direction of the arrow.
     */
    void setEnd(V vector);

    /**
     * Set the starting point of the arrow.
     *
     * @param point the starting point.
     */
    void setStart(P point);

    /**
     * Set the color of the arrow.
     *
     * @param color the color of the arrow.
     */
    void setColor(Color color);

}
