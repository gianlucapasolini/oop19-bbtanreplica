package view.bonus;

import element.Point2D;
import element.Point2DImpl;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import model.bonusblock.BonusBlock;

import java.util.Objects;

/**
 * This class represent the implementation of graphical bonus.
 */
public final class BonusViewImpl extends Circle implements BonusView {
    /**
     * bonusBlock representing the logic block.
     */
    private final BonusBlock logicBonus;

    /**
     * Factory method for the creation of the bonus.
     *
     * @param bonus representing the logic.
     * @return an implementation of BonusViewImpl
     */
    public static BonusViewImpl createBonusView(final BonusBlock bonus) {
        if (Objects.isNull(bonus)) {
            throw new IllegalStateException();
        }
        return new BonusViewImpl(bonus);
    }

    /**
     * Private constructor of the class.
     *
     * @param bonus representing the logic.
     */
    BonusViewImpl(final BonusBlock bonus) {
        super(bonus.getPosition().getX(), bonus.getPosition().getY(), bonus.getRadius());
        this.logicBonus = bonus;
        this.setFill(new ImagePattern(new Image(String.valueOf(ClassLoader.getSystemResource("images/" + this.logicBonus.getImg() + ".png")))));
    }

    @Override
    public Point2D getPosition() {
        return new Point2DImpl(this.getCenterX(), this.getCenterY());
    }

    @Override
    public void setPosition(final Point2D newPosition) {
        this.setCenterX(newPosition.getX());
        this.setCenterY(newPosition.getY());
        this.logicBonus.setPosition(newPosition);
    }

    @Override
    public BonusBlock getLogicBonus() {
        return this.logicBonus;
    }

}

