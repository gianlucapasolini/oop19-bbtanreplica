package testserialization;
import model.player.PlayersCollection;
import model.player.PlayersCollectionImpl;
import model.player.Player;
import model.player.PlayerBuilderImpl;
import model.serializator.Serializator;
import model.serializator.SerializatorFactory;
import model.serializator.SerializatorFactoryImpl;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Test;
import utility.BallColor;

import java.io.IOException;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;

public final class TestSerialization {

    private String path;
    private static final String FILENAME = "testPlayerSer.json";
    private final SerializatorFactory sf = new SerializatorFactoryImpl();
    private final PlayersCollection collection = new PlayersCollectionImpl();

    private void init() {
    	if (System.getProperty("os.name").contains("Windows")) {
            path = System.getenv("APPDATA") + System.getProperty("file.separator") + "BBTAN";
        } else {
            path = System.getenv("HOME") + System.getProperty("file.separator") + "BBTAN";
        }
        collection.addPlayer(new PlayerBuilderImpl().addNamePlayer("Daniele").addBestScore(45).build());
        collection.addPlayer(new PlayerBuilderImpl().addNamePlayer("Gianluca").addBestScore(50).build());
        collection.addPlayer(new PlayerBuilderImpl().addNamePlayer("Andrea").addBestScore(60).build());
        collection.addPlayer(new PlayerBuilderImpl().addNamePlayer("Riccardo").addBestScore(40).build());
        collection.addPlayer(new PlayerBuilderImpl().addNamePlayer("Alex").addBestScore(35).build());
    }

    @Test
    void testPlayerSerialization() throws IOException, ParseException {
        final Serializator<Player> spc = sf.playerSerializator();
        final Player player = new PlayerBuilderImpl().addNamePlayer("Daniele").build();
        init();
        player.setNewScore(20);
        player.addMoney(20);
        player.addNewColor(BallColor.AQUA_MARINE);
        player.addUnlockedRadius(10);

        spc.serialize(path, player, FILENAME);
        final Optional<Player> playerDeserialized = spc.deserialize(path, FILENAME);
        if (playerDeserialized.isEmpty()) {
            throw new IllegalStateException();
        }
        assertEquals(playerDeserialized.get().getBestScore(), player.getBestScore());
        assertEquals(playerDeserialized.get().getName(), player.getName());
    }

    @Test
    void testPlayersCollection() throws IOException, ParseException {
        final Serializator<PlayersCollection> serializator = this.sf.playersCollectionSerializator();
        init();
        serializator.serialize(path, collection, FILENAME);
        final Optional<PlayersCollection> collDeserialized = serializator.deserialize(path, FILENAME);
        if (collDeserialized.isEmpty()) {
           throw new IllegalStateException();
        }
        assertEquals(collDeserialized.get().getBestPlayer().get().getName(), "ANDREA");
    }

    @Test
    void testSecureSerialization() throws IOException, ParseException {
        final Serializator<PlayersCollection> serializator = this.sf.securePlayersCollectionSerializator();
        init();
        serializator.serialize(path, collection, FILENAME);
        final Optional<PlayersCollection> optAlteredColl = this.sf.playersCollectionSerializator().deserialize(path, FILENAME);
        if(optAlteredColl.isEmpty()) {
            return;
        }
        final PlayersCollection alteredColl = optAlteredColl.get();
        alteredColl.getPlayer("DANIELE").get().addMoney(500);
        final Serializator<PlayersCollection> insecureSerializator = this.sf.playersCollectionSerializator();
        insecureSerializator.serialize(path, alteredColl, FILENAME);
        final Optional<PlayersCollection> optSecureColl = serializator.deserialize(path, FILENAME);
        if(optSecureColl.isEmpty()) {
            return;
        }
        final PlayersCollection secureColl = optSecureColl.get();
        assertFalse(secureColl.containsPlayer("DANIELE"));
    }
}
