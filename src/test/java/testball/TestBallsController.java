package testball;

import controller.ballscontroller.*;
import controller.ballscontroller.time.TimeConverterUnit;
import controller.ballscontroller.time.TimeType;
import element.*;
import model.ball.Ball;
import model.ball.BallBuilder;
import model.ball.BallBuilderImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestBallsController {

    private static final int INTERVAL = 20;
    private static final int DELTA = 8;
    private static final int MAX = 100;
    private final Point2D start = Elements.ORIGIN;
    private final Vector2D vectorUp = new Vector2DImpl(0, 1);
    private final Vector2D vectorDown = new Vector2DImpl(0, -1);
    private final Point2D destination = new Point2DImpl(0, MAX);

    private BallsController buildController(final BallsControllerBuilder ballsControllerBuilder) {
        final BallBuilder ballBuilder = new BallBuilderImpl().addMovement(ball -> System.out.println("movement"));
        return ballsControllerBuilder.addBallsBuilder(ballBuilder)
                .addEndLaunch(() -> System.out.println("fine lancio"))
                .addStartPosition(start)
                .addTime(TimeType.ABSTRACT_TIME)
                .addPause(BallsControllerBuilder.Pause.PAUSE)
                .addTimeInterval(INTERVAL)
                .addNumberOfStepBetweenBalls(2)
                .addTimeMeasures(TimeConverterUnit.MILLISECOND)
                .build();
    }

    private void testBallsController(final BallsControllerBuilder controllerBuilder) throws InterruptedException {
        final BallsController controller = buildController(controllerBuilder);
        assertTrue(controller.areBallsStationary());
        assertEquals(1, controller.getBalls().size());
        controller.addBall();
        assertEquals(2, controller.getBalls().size());

        controller.getBalls().forEach(i -> i.setDestination(destination));
        controller.launchBalls(vectorUp);
        Thread.sleep(2 * INTERVAL + DELTA);
        assertFalse(controller.areBallsStationary());
        Thread.sleep(3 * INTERVAL + DELTA);
        assertEquals(1, controller.getBalls().stream().filter(Ball::isStationary).count());
        assertFalse(controller.areBallsStationary());
        Thread.sleep(2 * INTERVAL + DELTA);
        assertTrue(controller.areBallsStationary());
    }

    private void testPause(final BallsControllerBuilder controllerBuilder) throws InterruptedException {
        final BallsController controller = buildController(controllerBuilder);
        assertTrue(controller.areBallsStationary());
        assertEquals(1, controller.getBalls().size());
        controller.addBall();
        assertEquals(2, controller.getBalls().size());

        controller.getBalls().forEach(i -> i.setDestination(destination));
        controller.launchBalls(vectorUp);
        Thread.sleep(2 * INTERVAL + DELTA);
        assertFalse(controller.areBallsStationary());

        controller.pause();
        Thread.sleep(2 * INTERVAL + DELTA);
        assertTrue(controller.isPause());
        controller.restart();

        Thread.sleep(2 * INTERVAL + DELTA);
        assertEquals(1, controller.getBalls().stream().filter(Ball::isStationary).count());
        assertFalse(controller.areBallsStationary());
        Thread.sleep(2 * INTERVAL + DELTA);
        assertTrue(controller.areBallsStationary());

    }

    private void testBallsControllerTwice(final BallsControllerBuilder controllerBuilder) throws InterruptedException {
        final BallsController controller = buildController(controllerBuilder);
        assertTrue(controller.areBallsStationary());
        assertEquals(1, controller.getBalls().size());
        controller.getBalls().forEach(i -> i.setDestination(destination));

        controller.launchBalls(vectorUp);
        Thread.sleep(MAX / 2 + DELTA);
        assertFalse(controller.areBallsStationary());
        Thread.sleep(MAX / 2 + INTERVAL + DELTA);
        assertTrue(controller.areBallsStationary());
        controller.getBalls().forEach(i -> i.setDestination(destination.sum(vectorUp.scalarMultiplication(MAX))));
        controller.launchBalls(vectorUp);
        Thread.sleep(MAX / 2 + DELTA);
        assertFalse(controller.areBallsStationary());
        Thread.sleep(MAX / 2 + INTERVAL + DELTA);
        assertTrue(controller.areBallsStationary());
    }

    @Test
    void testBallsControllerSingleThread() throws InterruptedException {
        this.testBallsController(new BallsControllerBuilderImpl().addType(BallsControllerBuilder.Type.SINGLE_THREAD));
    }

    @Test
    void testPauseSingleThread() throws InterruptedException {
        this.testPause(new BallsControllerBuilderImpl().addType(BallsControllerBuilder.Type.SINGLE_THREAD));

    }

    @Test
    void testBallsControllerTwiceSingleThread() throws InterruptedException {
        this.testBallsControllerTwice(new BallsControllerBuilderImpl().addType(BallsControllerBuilder.Type.SINGLE_THREAD));
    }

    @Test
    void testBallsControllerMultiThread() throws InterruptedException {
        this.testBallsController(new BallsControllerBuilderImpl().addType(BallsControllerBuilder.Type.MULTI_THREAD));
    }

    @Test
    void testPauseMultiThread() throws InterruptedException {
        this.testPause(new BallsControllerBuilderImpl().addType(BallsControllerBuilder.Type.MULTI_THREAD));
    }

    @Test
    void testBallsControllerTwiceMultiThread() throws InterruptedException {
        this.testBallsControllerTwice(new BallsControllerBuilderImpl().addType(BallsControllerBuilder.Type.MULTI_THREAD));
    }

    @Test
    void testBallsControllerDown() throws InterruptedException {
        final BallsController controller = buildController(new BallsControllerBuilderImpl().addType(BallsControllerBuilder.Type.SINGLE_THREAD).addMinusAngleDegrees(-160));
        assertTrue(controller.areBallsStationary());
        assertEquals(1, controller.getBalls().size());
        controller.addBall();

        controller.getBalls().forEach(i -> i.setDestination(new Point2DImpl(0, -MAX)));
        controller.launchBalls(vectorDown);
        Thread.sleep(2 * INTERVAL + DELTA);
        assertFalse(controller.areBallsStationary());
        Thread.sleep(3 * INTERVAL + DELTA);
        assertEquals(1, controller.getBalls().stream().filter(Ball::isStationary).count());
        assertFalse(controller.areBallsStationary());
        controller.addBall();
        Thread.sleep(2 * INTERVAL + DELTA);
        assertTrue(controller.areBallsStationary());
        assertEquals(3, controller.getBalls().size());
    }
}
