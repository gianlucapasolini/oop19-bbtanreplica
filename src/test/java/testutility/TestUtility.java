package testutility;

import org.junit.jupiter.api.Test;
import utility.Counter;
import utility.CounterFactory;
import utility.IntegerCounterFactory;
import utility.Observer;
import utility.RangeFactory;
import utility.RangeFactoryImpl;
import utility.Source;
import utility.SourceImpl;
import utility.LimitCounterFactory;
import utility.LimitCounterFactoryImpl;

import java.util.Iterator;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestUtility {

    private final RangeFactory rangeFactory = new RangeFactoryImpl();
    private final CounterFactory<Integer> integerCounterFactory = new IntegerCounterFactory();

    @Test
    void testRangeIterator() {

        final Iterator<Integer> range = rangeFactory.standardRange(2).iterator();

        assertTrue(range.hasNext());
        assertEquals(0, range.next());

        assertTrue(range.hasNext());
        assertEquals(1, range.next());

        assertFalse(range.hasNext());

    }

    @Test
    void testWrongRange() {

        assertThrows(IllegalArgumentException.class, () -> rangeFactory.standardRangeFrom(2, 0));

    }

    @Test
    void testCountFrom0() {
        final int limit = 3;
        final Counter<Integer> counter = integerCounterFactory.standardCounter();
        assertEquals(0, counter.getValue());
        for (final int ignored : rangeFactory.standardRange(limit)) {
            counter.increment();
        }
        assertEquals(3, counter.getValue());
        counter.reset();
        assertEquals(0, counter.getValue());
    }

    @Test
    void testCountFromN() {
        final int start = 3;
        final int iteration = 3;
        final Counter<Integer> counter = integerCounterFactory.standardCounterFromValue(start);
        assertEquals(start, counter.getValue());
        for (final int ignored : rangeFactory.standardRange(iteration)) {
            counter.increment();
        }
        assertEquals(start + iteration, counter.getValue());
        counter.reset();
        assertEquals(start, counter.getValue());
    }

    @Test
    void testCountLimit() {
        final int start = 3;
        final int iteration = 3;
        final int max = 5;
        final LimitCounterFactory limitCounterFactory = new LimitCounterFactoryImpl();
        final Counter<Integer> counter = limitCounterFactory.limitCounter(integerCounterFactory.standardCounterFromValue(start), max);
        assertEquals(start, counter.getValue());
        for (final int ignored : rangeFactory.standardRange(iteration)) {
            counter.increment();
        }
        assertEquals(max, counter.getValue());
        counter.reset();
        assertEquals(start, counter.getValue());
    }

    @Test
    void testCountLimitReset() {
        final int start = 3;
        final int iteration = 3;
        final int max = 5;
        final LimitCounterFactory limitCounterFactory = new LimitCounterFactoryImpl();
        final Counter<Integer> counter = limitCounterFactory.limitCounterNotifyAndReset(integerCounterFactory.standardCounterFromValue(start), max,
                (source, value) -> assertEquals(max, value));
        assertEquals(start, counter.getValue());
        for (final int ignored : rangeFactory.standardRange(iteration)) {
            counter.increment();
        }
        assertEquals(max, counter.getValue());
    }

    @Test
    void testCountLimitException() {
        final int start = 3;
        final int iteration = 3;
        final int max = 5;
        final int step = -1;
        final LimitCounterFactory limitCounterFactory = new LimitCounterFactoryImpl();
        assertThrows(IllegalArgumentException.class, () -> limitCounterFactory.limitCounter(integerCounterFactory.counterFromValueWithStep(start, step), max));

        final Counter<Integer> counter = integerCounterFactory.counterFromValueWithStep(start + iteration, step);
        for (final int ignored : rangeFactory.standardRange(iteration)) {
            counter.increment();
        }
        assertThrows(IllegalArgumentException.class, () -> limitCounterFactory.limitCounter(counter, max));
    }

    @Test
    void testObserverAndSource() {
        final Source<Integer> s1 = new SourceImpl<>();
        final Source<Integer> s2 = new SourceImpl<>();
        final Integer integer = 2;

        final Observer<Integer> observer = (source, argument) -> {
            assertEquals(s2, source);
            assertEquals(integer, argument);
        };

        s1.addObserver((source, arg) -> {
            assertEquals(s1, source);
            assertEquals(integer, arg);
        });

        s1.notifyObservers(integer);

        s2.addObserver(observer);

        assertEquals(Set.of(observer), s2.getObserversSet());

        s2.removeObserver(observer);

        assertEquals(Set.of(), s2.getObserversSet());
    }
}
