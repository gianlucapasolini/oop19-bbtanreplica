package testcollisions.blocks;

import element.Point2DImpl;
import element.Vector2D;
import element.Vector2DImpl;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;
import model.ball.Ball;
import model.ball.BallBuilder;
import model.ball.BallBuilderImpl;
import model.collision.CollisionDetected;
import model.collision.CollisionManager;
import model.collision.CollisionManagerImpl;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestCollisionsWithBonus {
    private final BallBuilder builder = new BallBuilderImpl();
    List<Shape> blocks;
    Ball ball;
    CollisionManager collisionManagerCheck = new CollisionManagerImpl(null, 600, 350, 0, 0);

    private void setOneBonus(final int x, final int y) {
        blocks = List.of(new Circle(x,y,30));
        collisionManagerCheck.updateBlocks(blocks);
    }

    private void setBall(final int x, final int y, final int radius, final Vector2D dir) {
        ball = builder.addStartPosition(new Point2DImpl(x, y)).addRadius(radius).build();
        ball.setDirection(dir);
    }

    private void defaultTest(final Pair<Integer, Integer> blockPosition, final Pair<Integer, Integer> ballPosition,
                             final int ballRadius, final Vector2D ballDirection) {
        //default
        setOneBonus(blockPosition.getLeft(), blockPosition.getRight());
        setBall(ballPosition.getLeft(), ballPosition.getRight(), ballRadius, ballDirection);
        Optional<CollisionDetected> tmp;
        do {
            ball.moveByDistance(1);
            tmp = collisionManagerCheck.checkCollision(ball);
        } while (tmp.isEmpty());
        ball.collision(ball.getPosition(), ball.getDirection());
    }

    @Test
    void FirstTestBonus() {
        defaultTest(new ImmutablePair<>(50,50), new ImmutablePair<>(50, 100), 8, new Vector2DImpl(0, -1));
        System.out.print("Vertical CollisionManager From Bottom");
    }

    //ADD MORE TEST IN FUTURE
}
