package testcollisions.blocks;

import element.Point2DImpl;
import element.Vector2D;
import element.Vector2DImpl;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import model.collision.CollisionDetected;
import model.collision.CollisionManager;
import model.collision.CollisionManagerImpl;
import model.ball.Ball;
import model.ball.BallBuilder;
import model.ball.BallBuilderImpl;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestCollisionsWithBlocks {
    private final BallBuilder builder = new BallBuilderImpl();
    List<Shape> blocks;
    Ball ball;
    CollisionManager collisionManagerCheck = new CollisionManagerImpl(null, 600, 350, 0, 0);

    private void setOneBlock(final int x, final int y) {
        blocks = List.of(new Rectangle(x,y,50,50));
        collisionManagerCheck.updateBlocks(blocks);
    }

    private void setBall(final int x, final int y, final int radius, final Vector2D dir) {
        ball = builder.addStartPosition(new Point2DImpl(x, y)).addRadius(radius).build();
        ball.setDirection(dir);
    }

    private void defaultTest(final Pair<Integer, Integer> blockPosition, final Pair<Integer, Integer> ballPosition,
                             final int ballRadius, final Vector2D ballDirection) {
        //default
        setOneBlock(blockPosition.getLeft(), blockPosition.getRight());
        setBall(ballPosition.getLeft(), ballPosition.getRight(), ballRadius, ballDirection);
        Optional<CollisionDetected> tmp;
        do {
            ball.moveByDistance(1);
            tmp = collisionManagerCheck.checkCollision(ball);
        } while (tmp.isEmpty());
        ball.collision(tmp.get().getNewCenterPosition().get(), tmp.get().getNewDirection().get());
        tmp = collisionManagerCheck.checkCollision(ball);
        assertTrue(tmp.isEmpty());
    }

    //------------------ BOTTOM COLLISION -------------------------------------

    @Test
    void testVerticalCollisionFromBottom() {
        //from bottom
        defaultTest(new ImmutablePair<>(0,50), new ImmutablePair<>(25, 110), 8, new Vector2DImpl(0, -1));
        System.out.print("Vertical CollisionManager From Bottom");
    }

    @Test
    void testNotVerticalCollisionFromBottomLeft() {
        //from bottom
        defaultTest(new ImmutablePair<>(0,50), new ImmutablePair<>(15, 110), 8, new Vector2DImpl(1, -1));
        System.out.print("Not Vertical CollisionManager From Bottom Left");
    }

    @Test
    void testNotVerticalCollisionFromBottomRight() {
        //from bottom
        defaultTest(new ImmutablePair<>(0,50), new ImmutablePair<>(40, 110), 8, new Vector2DImpl(-1, -1));
        System.out.print("Not Vertical CollisionManager From Bottom Right");
    }

    //------------------ TOP COLLISION -------------------------------------

    @Test
    void testVerticalCollisionFromTop() {
        //from top
        defaultTest(new ImmutablePair<>(0,50), new ImmutablePair<>(25, 30), 8, new Vector2DImpl(0, 1));
        System.out.print("Vertical CollisionManager From Top");
    }

    @Test
    void testNotVerticalCollisionFromTopLeft() {
        //from top
        defaultTest(new ImmutablePair<>(0,50), new ImmutablePair<>(15, 30), 8, new Vector2DImpl(1, 1));
        System.out.print("Not Vertical CollisionManager From Top Left");
    }

    @Test
    void testNotVerticalCollisionFromTopRight() {
        //from top
        defaultTest(new ImmutablePair<>(0,50), new ImmutablePair<>(40, 30), 8, new Vector2DImpl(-1, 1));
        System.out.print("Not Vertical CollisionManager From Top Right");
    }

    //------------------ RIGHT COLLISION -------------------------------------

    @Test
    void testHorizontalCollisionFromRight() {
        //from right
        defaultTest(new ImmutablePair<>(0,50), new ImmutablePair<>(70, 75), 8, new Vector2DImpl(-1, 0));
        System.out.print("Vertical CollisionManager From Right");
    }

    @Test
    void testNotHorizontalCollisionFromRightTop() {
        //from right
        defaultTest(new ImmutablePair<>(0,50), new ImmutablePair<>(60, 75), 8, new Vector2DImpl(-0.3, 1.5));
        System.out.print("Not Vertical CollisionManager From Right Top");
    }

    @Test
    void testNotHorizontalCollisionFromRightBottom() {
        //from right
        defaultTest(new ImmutablePair<>(0,50), new ImmutablePair<>(70, 75), 8, new Vector2DImpl(-1, -1));
        System.out.print("Not Vertical CollisionManager From Right Bottom");
    }

    //------------------ LEFT COLLISION -------------------------------------

    @Test
    void testHorizontalCollisionFromLeft() {
        //from left
        defaultTest(new ImmutablePair<>(50,50), new ImmutablePair<>(20, 75), 8, new Vector2DImpl(1, 0));
        System.out.print("Vertical CollisionManager From Left");
    }

    @Test
    void testNotHorizontalCollisionFromLeftTop() {
        //from left
        defaultTest(new ImmutablePair<>(50,50), new ImmutablePair<>(20, 75), 8, new Vector2DImpl(1, 1));
        System.out.print("Not Vertical CollisionManager From Left Top");
    }

    @Test
    void testNotHorizontalCollisionFromLeftBottom() {
        //from right
        defaultTest(new ImmutablePair<>(50,50), new ImmutablePair<>(20, 75), 8, new Vector2DImpl(1, -1));
        System.out.print("Not Vertical CollisionManager From Left Bottom");
    }

}
