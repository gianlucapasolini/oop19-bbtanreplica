package testcollisions.blocks;

import element.Point2DImpl;
import element.Vector2D;
import element.Vector2DImpl;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import model.ball.Ball;
import model.ball.BallBuilder;
import model.ball.BallBuilderImpl;
import model.collision.CollisionDetected;
import model.collision.CollisionManager;
import model.collision.CollisionManagerImpl;
import org.junit.jupiter.api.Test;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestCollisionWith2Blocks {
    private final BallBuilder builder = new BallBuilderImpl();
    List<Shape> blocks;
    Ball ball;
    CollisionManager collisionManagerCheck = new CollisionManagerImpl(null, 600, 350, 0, 0);

    private void setTwoBlocks(final int x1, final int y1, final int x2, final int y2) {
        blocks = List.of(new Rectangle(x1,y1,50,50), new Rectangle(x2, y2, 50, 50));
        collisionManagerCheck.updateBlocks(blocks);
    }

    private void setBall(final int x, final int y, final int radius, final Vector2D dir) {
        ball = builder.addStartPosition(new Point2DImpl(x, y)).addRadius(radius).build();
        ball.setDirection(dir);
    }

    private void defaultTest2Blocks(final int x1, final int y1, final int x2, final int y2, final int x, final int y,
                               final int radius, final double dirX, final double dirY ){
        setTwoBlocks(x1, y1, x2, y2);
        setBall(x, y, radius, new Vector2DImpl(dirX, dirY));
        Optional<CollisionDetected> tmp;
        do {
            ball.moveByDistance(1);
            tmp = collisionManagerCheck.checkCollision(ball);
        } while (tmp.isEmpty());
        ball.collision(tmp.get().getNewCenterPosition().get(), tmp.get().getNewDirection().get());
        assertEquals(tmp.get().getBlocks().get().size(), 2);
        tmp = collisionManagerCheck.checkCollision(ball);
        assertTrue(tmp.isEmpty());
    }

    @Test
    void testCollisionWith2BlocksFromBottom() {
        //from bottom
        defaultTest2Blocks(0, 50, 50, 50, 50, 115, 8, 0, -1);
        System.out.print("Vertical CollisionManager From Bottom");
    }

    @Test
    void testCollisionWith2BlocksFromTop() {
        //from top
        defaultTest2Blocks(0, 50, 50, 50, 50, 30, 8, 0, 1);
        System.out.print("Vertical CollisionManager From Top");
    }

    @Test
    void testCollisionWith2BlocksFromLeft() {
        //from left
        defaultTest2Blocks(50, 50, 50, 100, 30, 100, 8, 1, 0);
        System.out.print("Horizontal CollisionManager From Left");
    }

    @Test
    void testCollisionWith2BlocksFromRight() {
        //from right
        defaultTest2Blocks(50, 50, 50, 100, 130, 100, 8, -1, 0);
        System.out.print("Horizontal CollisionManager From Right");
    }
}
