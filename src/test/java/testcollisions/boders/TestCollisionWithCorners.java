package testcollisions.boders;

import element.Point2D;
import element.Point2DImpl;
import element.Vector2D;
import element.Vector2DImpl;
import model.ball.Ball;
import model.ball.BallBuilder;
import model.ball.BallBuilderImpl;
import model.collision.CollisionDetected;
import model.collision.CollisionManager;
import model.collision.CollisionDetectedImpl;
import model.collision.CollisionManagerImpl;
import org.junit.jupiter.api.Test;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestCollisionWithCorners {
    private final BallBuilder builder = new BallBuilderImpl();
    CollisionManager collisionManagerCheck = new CollisionManagerImpl(null, 500, 350, 0, 0);

    private Optional<CollisionDetectedImpl> bottomCollision(final Point2D newPos) {
        return Optional.of(new CollisionDetectedImpl(newPos));
    }

    @Test
    void testCornerTopRight() {
        int x = 4, y= 496;
        Point2D position = new Point2DImpl(x, y);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(-1, 1));
        //3
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //2.3
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //1.6
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //0.9
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //0.2-
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //-0.5
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, bottomCollision(new Point2DImpl(1,499)));
            if (collision.get().getNewCenterPosition().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), new Vector2DImpl(0, 0));
            }
        }
    }

    @Test
    void testCornerTopLeft() {
        int x = 346, y= 496;
        Point2D position = new Point2DImpl(x, y);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(1, 1));
        //3
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //2.3
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //1.6
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //0.9
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //0.2
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //-0.5
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, bottomCollision(new Point2DImpl(349,499)));
            if (collision.get().getNewCenterPosition().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), new Vector2DImpl(0, 0));
            }
        }
    }

    private Optional<CollisionDetectedImpl> generalBorderCollision(final Point2D newPos, final Vector2D newVec) {
        return Optional.of(new CollisionDetectedImpl(newVec.getNormalizedVector(), newPos));
    }

    @Test
    void testCornerBottomRight() {
        int x = 4, y= 4;
        Point2D position = new Point2DImpl(x, y);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(-1, -1));
        //3
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //2.3
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //1.6
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //0.9
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //0.2
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //-0.5
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, generalBorderCollision(new Point2DImpl(1,1), new Vector2DImpl(1, 1)));
            if (collision.get().getNewCenterPosition().isPresent() && collision.get().getNewDirection().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), collision.get().getNewDirection().get());
            }
        }
    }

    @Test
    void testCornerBottomLeft() {
        int x = 346, y= 4;
        Point2D position = new Point2DImpl(x, y);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(1, -1));
        //3
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //2.3
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //1.6
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //0.9
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //0.2
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //-0.5
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, generalBorderCollision(new Point2DImpl(349,1), new Vector2DImpl(-1, 1)));
            if (collision.get().getNewCenterPosition().isPresent() && collision.get().getNewDirection().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), collision.get().getNewDirection().get());
            }
        }
    }
}