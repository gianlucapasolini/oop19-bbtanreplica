## BBtanReplica

Il videogioco consiste in un clone del classico gioco per smartphone BBTAN realizzato per il corso di **Programmazione ad oggetti** dell'Università  di Bologna, sede di Cesena.

---
## Informazioni:

**Avvio**

1. Scaricare **oop19-bbtanreplica-all.jar** nella sezione downloads.
2. Per lanciare l'applicazione eseguire il comando 'java -jar oop19-bbtanreplica-all.jar' da terminale.
3. Iniziare a giocare!

**Relazione**

Per visionare la relazione è sufficiente scaricarla nella sezione downloads.

**JAVADOC**

Per visionare la JAVADOC è sufficiente scaricarla nella sezione downloads.

---

## Credits:

* Albertini Riccardo
* Brighi Andrea
* Di Lillo Daniele
* Giorgetti Alex
* Pasolini Gianluca
